class MemberPresenter < Burgundy::Item	

	def show_image
		h.image_tag(picture_url)
	end

	def full_profile
		h.content_tag(:div, show_thumb + profile + more_button, class: 'teacher-profile row')
	end

	def more_button
		'<a ng-click="moreProfile()" class="more pull-right"><img src="images/more-down-white.jpg" alt="Ver perfil"></a>'
	end

	def name_title
		h.content_tag(:h5, item.name)
	end

	def profile
		h.content_tag(:div, name_title + item.profile, class: 'col-md-9 col-sm-9 col-xs-8')
	end

	def raw_profile
		item.profile
	end

	def show_thumb
		h.content_tag(:figure, h.image_tag(thumb_url), class: 'col-md-3 col-sm-3 col-xs-4')
	end

	def picture_url
		item.image.url
	end

	def thumb_url
		item.image.thumb.url
	end

	def has_image?
		item.image?
	end

end