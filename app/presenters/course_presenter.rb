class CoursePresenter < Burgundy::Item
	def purpose
		if item.purpose.present?
			h.content_tag(:div, h.raw(item.purpose), class: 'text')
		end
	end

	def introduction
		if item.introduction.present?
			h.content_tag(:div, h.raw(item.introduction), class: 'text')
		end
	end

	def about
		if item.about.present?
			h.content_tag(:div, h.raw(item.about), class: 'text')
		end		
	end

	def modality_slug
		item.modality.slug
	end

	def modality
		item.modality.title
	end

	def member
		full_profile
	end

	def full_profile
		h.content_tag(:div, h.raw(show_thumb + profile + more_button), class: 'teacher-profile row')
	end

	def more_button
		h.raw('<a class="more pull-right"><img ng-click="moreProfile()" src="assets/more-down-white.jpg" alt="Ver perfil"></a>')
	end

	def name_title
		h.content_tag(:h5, item.member.name)
	end

	def profile
		h.content_tag(:div, name_title + h.raw(item.member.profile), class: 'col-md-9 col-sm-9 col-xs-8')
	end

	def show_thumb
		h.content_tag(:figure, h.image_tag(member_thumb_url), class: 'col-md-3 col-sm-3 col-xs-4')
	end	

	def member_thumb_url
		item.member.image.thumb.url
	end

	def thumbnail_url
		item.thumbnail.thumb.url
	end

	def header_url
		item.header_image.header.url
	end

	def important_note
		if item.important_note.present?
			h.content_tag(:div, h.raw(item.important_note), class: 'text')
		end
	end

	def value
		if item.value.present?
			h.number_to_currency(item.value)
		end
	end

	def discount
		if item.discount.present?
			h.number_to_currency(item.discount)
		end
	end
end