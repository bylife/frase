class ServicePresenter < Burgundy::Item
	def url
		"services/#{item.slug}"
	end

	def body
		if item.body.present?
			h.content_tag(:div, h.raw(item.body), class: 'text')
		end
	end

	def show_image
		h.image_tag(picture_url)
	end	

	def show_thumb
		h.image_tag(thumb_url)
	end

	def header_image_url
		item.header_image.header.url
	end

	def picture_url
		item.image.url
	end

	def thumb_url
		item.image.thumb.url
	end

	def has_image?
		item.image?
	end
end