class JobPresenter < Burgundy::Item
	def url
		"portfolio/#{item.slug}"
	end

	def type
		item.type.title
	end

	def show_image
		h.image_tag(picture_url)
	end

	def show_thumb
		h.image_tag(thumb_url)
	end

	def picture_url
		item.image.url
	end

	def header_url
		item.image.header.url
	end

	def thumb_url
		item.image.thumb.url
	end

	def has_image?
		item.image?
	end

	def need
		if item.need.present?			
			h.content_tag(:div, h.raw(item.need), class: 'text')			
		end
	end

	def solution
		if item.solution.present?			
			h.content_tag(:div, h.raw(item.solution), class: 'text')			
		end
	end

	def result
		if item.result.present?			
			h.content_tag(:div, h.raw(item.result), class: 'text')			
		end
	end

	def technical_form
		if item.technical_form.present?			
			h.content_tag(:div, h.raw(item.technical_form), class: 'text')			
		end
	end

end