class CustomerPresenter < Burgundy::Item
	def jobtype
		case item.jobtype
		when "courses"
			"Treinamentos"
		when "services"
			"Serviços"
		end
	end

	def show_image
		h.image_tag(picture_url)
	end

	def show_thumb
		h.image_tag(thumb_url)
	end

	def picture_url
		item.image.url
	end

	def thumb_url
		item.image.thumb.url
	end

	def has_image?
		item.image?
	end
end