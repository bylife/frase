class PostPresenter < Burgundy::Item
	def the_content
		if item.body.present?
			h.content_tag(:div, h.raw(item.body), class: 'text')
		end
	end

	def the_content
		if item.body.present?
			h.content_tag(:div, h.raw(item.body), class: 'text')
		end
	end

	def the_excerpt
		if item.body.present?
			h.content_tag(:div, h.truncate(item.body, length: 50, escape: false), class: 'text-excerpt')
		end
	end

	def username
		if item.user.present?
			item.user.full_name
		end
	end

	def tag_list
		h.raw item.tag_list.map { |t| h.link_to t, Rails.application.routes.url_helpers.tag_path(t) }.join(', ')		
	end

	def category
		if item.category.present?
			item.category
		end
	end

	def show_thumbnail
		h.image_tag(picture_url)
	end

	def show_thumb
		h.image_tag(thumb_url)
	end

	def picture_url
		item.thumbnail.url
	end

	def thumb_url
		item.thumbnail.thumb.url
	end

	def has_image?
		item.thumbnail?
	end
end