class ClassroomPresenter < Burgundy::Item
	def course
		item.course.title
	end

	def course_slug
		item.course.slug
	end

	def installments	
		PagSeguro.environment = "production" #or sandbox
		PagSeguro.encoding = "UTF-8"
		PagSeguro::Installment.find(item.value.to_s, 'visa')
	end
end