'use strict'

###*
 # @ngdoc directive
 # @name fraseApp.directive:contactService
 # @description
 # # contactService
###
angular.module 'fraseApp'
  .directive 'contactService', ->
    restrict: 'EA'
    templateUrl: '_contact-service.html'
