'use strict'

###*
 # @ngdoc directive
 # @name fraseApp.directive:missionBox
 # @description
 # # missionBox
###
angular.module 'fraseApp'
  .directive 'missionBox', ->
    restrict: 'EA'
    templateUrl: '_mission-box.html'
    replace: true
