'use strict'

###*
 # @ngdoc directive
 # @name fraseApp.directive:videoBox
 # @description
 # # videoBox
###
angular.module 'fraseApp'
  .directive 'videoBox', ->
    restrict: 'EA'
    templateUrl: '_video-box.html'
    replace: true
