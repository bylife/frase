'use strict'

###*
 # @ngdoc directive
 # @name fraseApp.directive:sidebar
 # @description
 # # Sidebar
###
angular.module 'fraseApp'
  .directive 'sidebarBlog', ['Category', 'Post', 'Classroom', '$rootScope', (Category, Post, Classroom, $rootScope, $http) ->
	restrict: 'EA'
	templateUrl: '_sidebar.html'
	controller: ($scope) ->
		Classroom.query({action: 'nextcourses'}).$promise.then (data) ->
			$scope.next_courses = data
		
		Post.query({action: 'publisheds'}).$promise.then (data) ->
			$scope.recent_posts = data
		$scope.archive = []
		Category.query().$promise.then (data) ->
			$scope.categories = data
]