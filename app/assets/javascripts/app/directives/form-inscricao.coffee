'use strict'

###*
 # @ngdoc directive
 # @name fraseApp.directive:formInscricao
 # @description
 # # formInscricao
###
angular.module 'fraseApp'
  .directive 'formInscricao', ->
	restrict: 'EA'
	templateUrl: '_form-inscricao.html'
	controller: ($scope, $resource, Student) ->
		$scope.Student 		= new Student()
		$scope.Subscription = {}
		$scope.step 		= 1

		$scope.nextStep = () ->
			$scope.step++

		$scope.prevStep = () ->
			$scope.step--

		$scope.selectStep = (id) ->
			$scope.step = id

		$scope.saveSubscription = () ->
			console.log $scope.Student
			console.log $scope.Subscription			
			Student.save {student: $scope.Student}, (data) ->
				console.log data