'use strict'

###*
 # @ngdoc directive
 # @name fraseApp.directive:valuesBox
 # @description
 # # valuesBox
###
angular.module 'fraseApp'
  .directive 'valuesBox', ->
    restrict: 'EA'
    templateUrl: '_values-box.html'
    replace: true
