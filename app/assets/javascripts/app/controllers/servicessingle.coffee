'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:ServicessingleCtrl
 # @description
 # # ServicessingleCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
  .controller 'ServicesSingleCtrl', ['Service', '$scope', '$routeParams', '$rootScope', '$location',(Service, $scope, $routeParams, $rootScope,$location) ->
	$rootScope.change_logo($location.path())
	$scope.services = Service.query()	
	load = () ->
		slug = $routeParams.slug;
		data = Service.get({slug: slug})
		if data
			$scope.service = data
			console.log data
		else
			console.log 'Falha ao carregar serviços!'

	load()
]