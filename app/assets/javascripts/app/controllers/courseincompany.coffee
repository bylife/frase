'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:CoursesCtrl
 # @description
 # # CoursesCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
	.controller 'CourseInCompanyCtrl', ['$scope', 'Course', 'Modality', 'Header', '$rootScope', '$location', '$resource', ($scope, Course, Modality, Header, $rootScope, $location, $resource) ->
		$scope.header = Header.query({page: 'curso-in-company'})
		$rootScope.change_logo($location.path())
		Modality = $resource('/api/modalities/:q.json', {q: "@q"}, {'update': {method: 'PUT'}})		
		Modality.get({q: '2'}).$promise.then (data) ->
			$scope.modality = data

]