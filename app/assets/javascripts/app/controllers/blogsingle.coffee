'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:BlogCtrl
 # @description
 # # BlogCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
  .controller 'BlogSingleCtrl', ['$scope', 'Post', 'Header', '$routeParams', '$rootScope', '$location', ($scope, Post, Header, $routeParams, $rootScope, $location) ->
	$rootScope.change_logo($location.path())	
	$scope.header = Header.query({page: 'blog'})

	load = () ->
		slug = $routeParams['slug']
		Post.get({slug: slug}).$promise.then (data) ->
			console.log data
			$scope.post  = data
	load()
	#$scope.totalItems = $scope.posts.length
	$scope.currentPage = 1
	$scope.setPage = (pageNo) ->
		$scope.currentPage = pageNo
]