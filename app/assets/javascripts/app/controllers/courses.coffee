'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:CoursesCtrl
 # @description
 # # CoursesCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
	.controller 'CoursesCtrl', ['$scope', 'Course', 'Modality', 'Header', '$rootScope', '$location',($scope, Course, Modality, Header, $rootScope, $location) ->
		$scope.modalities = Modality.query()
		$scope.header = Header.query({page: 'cursos'})
		$rootScope.change_logo($location.path())

		$scope.goIncompany = (slug) ->
			if slug == 'in-company'
				$location.path('curso-in-company')
]