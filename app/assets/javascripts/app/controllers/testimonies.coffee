'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:TestimoniesCtrl
 # @description
 # # TestimoniesCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
	.controller 'TestimoniesCtrl', ['$scope', 'Testimony','$rootScope', 'Header', '$location', ($scope, Testimony, $rootScope, Header, $location) ->
		$rootScope.change_logo($location.path())
		$scope.testimonies = Testimony.query()
		$scope.header = Header.query({page: 'depoimentos'})

		$scope.totalItems = $scope.testimonies.length

		$scope.currentPage = 1

		$scope.setPage = (pageNo) ->
			$scope.currentPage = pageNo
]