'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:ContactCtrl
 # @description
 # # ContactCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
  .controller 'ContactCtrl', [ '$scope', '$rootScope', 'Header', '$location',($scope, $rootScope, Header, $location) ->
	$rootScope.change_logo($location.path())
	$scope.header = Header.query({page: 'contato'})
]