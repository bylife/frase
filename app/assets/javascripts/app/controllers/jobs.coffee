'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:JobsCtrl
 # @description
 # # JobsCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
	.controller 'JobsCtrl', ['Type', 'Job', '$scope', 'Header', '$rootScope', '$location', (Type, Job, $scope, Header, $rootScope, $location) ->
		$rootScope.change_logo($location.path())
		all_jobs = []
		Type.query().$promise.then (data) ->
			$scope.other_jobs = data
			angular.forEach data, (value, key) ->
				if value.jobs.length > 0
					angular.forEach value.jobs, (v, k) ->
						all_jobs.push v

		$scope.types = [
			title: 'Todos',
			jobs: all_jobs
		]

		$scope.$watch 'other_jobs', () ->
			angular.forEach $scope.other_jobs, (v, k) ->
				if v.jobs.length > 0
					$scope.types.push v
					
		$scope.header = Header.query({page: 'cases'})
]