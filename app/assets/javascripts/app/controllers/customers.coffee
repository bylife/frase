'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:CustomersCtrl
 # @description
 # # CustomersCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
	.controller 'CustomersCtrl', ['Customer', 'Header', '$rootScope', '$scope', '$resource', '$location', (Customer, Header, $rootScope, $scope, $resource, $location) ->
		$rootScope.change_logo($location.path())
		$scope.customers = Customer.query({
			jobtype: 'Serviços'
		})
		$scope.header = Header.query({page: 'quem-atendemos'})

		$scope.trainers = Customer.query({
			jobtype: 'Treinamentos'
		})
]