'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:BlogCtrl
 # @description
 # # BlogCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
  .controller 'BlogCtrl', ['$scope', 'Post', 'Header', '$rootScope', '$location', ($scope, Post, Header, $rootScope, $location) ->
	$rootScope.change_logo($location.path())
	$scope.header = Header.query({page: 'blog'})
	$scope.posts = Post.query({action: 'publisheds'})
	
	$scope.totalItems = $scope.posts.length
	$scope.currentPage = 1
	$scope.setPage = (pageNo) ->
		$scope.currentPage = pageNo
]