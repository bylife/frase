'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:TalentsCtrl
 # @description
 # # TalentsCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
  .controller 'TalentsCtrl', ['$scope', '$rootScope', '$location', 'Talent', 'Header', ($scope, $rootScope, $location, Talent, Header) ->
	$rootScope.change_logo($location.path())
	#$scope.header = Header.query({page: 'banco-de-talentos'})
	all_talents = []
	Talent.query().$promise.then (data) ->
		$scope.other_talents = data
		angular.forEach data, (value, key) ->
			if value.talents.length > 0
				angular.forEach value.talents, (v, k) ->
					all_talents.push(v)
	
	$scope.talents = [
		letter: 'TODOS'
		talents: all_talents,		
	]

	$scope.$watch 'other_talents', () ->
		angular.forEach $scope.other_talents, (v, k) ->
			if v.talents.length > 0
				$scope.talents.push v
	

]