'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:JobssingleCtrl
 # @description
 # # JobssingleCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'  
  .controller 'JobsSingleCtrl', ['$scope', '$rootScope', '$routeParams', 'Header', 'Type', 'Job', '$location', ($scope, $rootScope, $routeParams, Header, Type, Job, $location) ->
	$rootScope.change_logo($location.path())
	$scope.header = Header.query({page: 'cases'})
	slug = $routeParams.slug
	$scope.job = Job.get({slug: slug})
	console.log $scope.job

	Job.query().$promise.then (data) ->
		data.splice($scope.job, 1)
		$scope.jobs = data
]
