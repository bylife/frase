'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:ServicesCtrl
 # @description
 # # ServicesCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
  .controller 'ServicesCtrl', ['$scope', 'Service', '$rootScope', 'Header', '$location', ($scope, Service, $rootScope, Header, $location) ->
	$scope.services = Service.query()
	$rootScope.change_logo($location.path())
	$scope.header = Header.query({page: 'servicos'})
]