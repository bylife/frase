'use strict'

###*
 # @ngdoc function
 # @name fraseApp.controller:SubscriptionsCtrl
 # @description
 # # SubscriptionsCtrl
 # Controller of the fraseApp
###
angular.module 'fraseApp'
  .controller 'SubscriptionsCtrl', ['$scope', '$rootScope', '$routeParams', 'Course', 'Classroom', '$location', ($scope, $rootScope, $routeParams, Course, Classroom, $location) ->
  	$rootScope.change_logo($location.path())
  	getCourse = () ->
  		Course.get({slug: $routeParams['slug']}).$promise.then (data) ->
  			$scope.course = data  			
  	getCourse()

  	getClassroom = () ->
  		Classroom.get({id: $routeParams['classroom']}).$promise.then (data) ->
  			$scope.classroom = data
  	getClassroom()
]