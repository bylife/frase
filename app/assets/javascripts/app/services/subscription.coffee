'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Subscription
 # @description
 # # Subscription
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Subscription', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->  
	Subscription = $resource('/api/subscriptions/:id.json', {id: '@id'}, {'update': {method: 'PUT'}})
]