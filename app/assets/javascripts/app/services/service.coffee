'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Service
 # @description
 # # Service
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Service', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/services/:slug.json', {slug: "@slug"}, {update: {method: "PUT"}})
]