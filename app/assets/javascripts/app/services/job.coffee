'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Job
 # @description
 # # Job
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Job', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
    $http.defaults.useXDomain = true
    $resource('/api/jobs/:slug.json', {slug: "@slug"}, {'update': method: 'PUT'})
]

