'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Customer
 # @description
 # # Customer
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Customer', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
    $http.defaults.useXDomain = true
    $resource('/api/customers/:id.json', {id: "@id"}, {'update': method: 'PUT'})    
]
