'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Classrooms
 # @description
 # # Classrooms
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Classroom', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/classrooms/:action/:id.json', {action: '@action', id: '@id'}, {'update': {method: 'PUT'}})
]