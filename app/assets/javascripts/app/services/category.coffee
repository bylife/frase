'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Category
 # @description
 # # Category
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'Category', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/categories/:id.json', {id: "@id"}, {'update': method: 'PUT'})
]