'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Member
 # @description
 # # Member
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'Member', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/members/:id.json', {id: "@id"}, {'update': method: 'PUT'})
]