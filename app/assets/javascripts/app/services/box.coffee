'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Box
 # @description
 # # Box
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'Box', ['$resource', '$http', ($resource, $http) ->
    $http.defaults.useXDomain = true
    $resource('/api/boxes/:id.json', null, {'update': method: 'PUT'})
]