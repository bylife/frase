'use strict'

###*
 # @ngdoc service
 # @name fraseApp.CoursesApi
 # @description
 # # CoursesApi
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'CoursesApi', ['$http', '$rootScope', 'ajax', ($http, $rootScope, ajax) ->
	get_courses: () ->
		console.log 'getting courses...'
		ajax.get $rootScope.appUrl + 'mock/courses.json'
	get_course: (params) ->
		console.log 'getting course...'
		ajax.get $rootScope.appUrl + 'mock/courses.json', params
]