'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Modality
 # @description
 # # Modality
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'Modality', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/modalities/courses/:q.json', {q: "@q"}, {'update': {method: 'PUT'}})
]