'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Post
 # @description
 # # Post
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Post', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
  	$http.defaults.useXDomain = true
  	$resource('/api/posts/:action/:slug.json', {action: '@action', slug: '@slug'}, {'update': method: 'PUT'})
]
