'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Testimony
 # @description
 # # Testimony
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Testimony', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/testimonies/:id.json', { id: '@id'},
		{
			'index'		: method: 'GET', 	isArray : true
			'show'		: method: 'GET', 	isArray : false
			'create'	: method: 'POST'
			'update'	: method: 'PUT'
			'destroy'	: method: 'DELETE'
		}
	)
]
