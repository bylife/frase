'use strict'

###*
 # @ngdoc service
 # @name fraseApp.ajax
 # @description
 # # ajax
 # Factory in the fraseApp.
###
angular.module 'modajax', []
  .factory 'ajax', ['$http', ($http) ->
	get: (url, params) ->
		if !params then params = {}
		$http
			method: 'GET'
			url: url
			params: params

	post: (url, params) ->
		if !params then params = {}		
		$http
			method: 'POST'
			url: url
			data: params
	
	put: (url, params) ->
		if !params then params = {}		
		$http
			method: 'PUT'
			url: url
			data: params

	del: (url, params) ->
		if !params then params = {}		
		$http
			method: 'DELETE'
			url: url
			data: params
]