'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Type
 # @description
 # # Type
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'Type', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/types/:slug.json', {slug: "@slug"}, {update: {method: "PUT"}})
]
