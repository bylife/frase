'use strict'

###*
 # @ngdoc service
 # @name fraseApp.modajax
 # @description
 # # modajax
 # Factory in the fraseApp.
###
angular.module 'modajax', []
  .factory 'ajax', ['$http', ($http) ->
	get: (url, params) ->
		if !params then params = {}
		$http
			method: 'GET'
			url: url
			params: params

	post: (url, params) ->
		if !params then params = {}		
		$http
			method: 'POST'
			url: url
			data: params
]