'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Header
 # @description
 # # Header
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'Header', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	#$resource($rootScope.apiUrl+'api/headers/:id.json', {id: "@id"}, {'update': method: 'PUT'})
	$resource('/api/headers/page/:page.json', {page: "@page"}, {'update': method: 'PUT'})
]