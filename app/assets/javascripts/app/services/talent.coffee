'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Talent
 # @description
 # # Talent
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Talent', ['$resource', '$http', ($resource, $http) ->
    $http.defaults.useXDomain = true
    $resource('/api/talents/categories.json', null, {'update': method: 'PUT'})
]