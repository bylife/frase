'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Course
 # @description
 # # Course
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Course', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->
	$http.defaults.useXDomain = true
	$resource('/api/courses/:slug.json', {slug: '@slug'}, {'update': {method: 'PUT'}})
]