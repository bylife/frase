'use strict'

###*
 # @ngdoc service
 # @name fraseApp.PostsApi
 # @description
 # # PostsApi
 # Service in the fraseApp.
###
angular.module 'fraseApp'
  .service 'PostsApi', ['$http', '$rootScope', 'ajax', ($http, $rootScope, ajax) ->
	get_posts: () ->
		console.log 'getting posts...'
		ajax.get $rootScope.appUrl + 'mock/posts.json'
	get_post: (params) ->
		console.log 'getting post...'
		ajax.get $rootScope.appUrl + 'mock/posts.json', params
]