'use strict'

###*
 # @ngdoc service
 # @name fraseApp.Student
 # @description
 # # Student
 # Factory in the fraseApp.
###
angular.module 'fraseApp'
  .factory 'Student', ['$resource', '$rootScope', '$http', ($resource, $rootScope, $http) ->  	
  	$resource('/api/students/:id.json', {id: '@id'}, {'update': {method: 'PUT'}})			
]
