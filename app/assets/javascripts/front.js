// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sass-official/assets/javascripts/bootstrap-sprockets
//= require angular/angular
//= require angular-route/angular-route.min
//= require angular-resource/angular-resource.min
//= require angular-animate/angular-animate.min
//= require angular-cookies/angular-cookies.min
//= require angular-sanitize/angular-sanitize.min
//= require angular-touch/angular-touch.min
//= require angular-mousewheel/mousewheel
//= require moment
//= require angular-i18n/angular-locale_pt-br
//= require angular-rails-templates
//= require angular-ui-utils/ui-utils.min
//= require angular-bootstrap/ui-bootstrap.min
//= require angular-bootstrap/ui-bootstrap-tpls.min
//= require ngmap/build/scripts/ng-map
//= require ng-covervid/ngCovervid.min
//= require anguvideo/js/anguvideo
//= require lodash/dist/lodash.min
//= require hamsterjs/hamster
//= require kinetic-slider/gilbox-kinetic-slider
//= require_tree ../templates
//= require_tree ./app