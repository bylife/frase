class Post < ActiveRecord::Base
	extend FriendlyId
	mount_uploader :thumbnail, PictureUploader
	belongs_to :user
	belongs_to :category
	acts_as_taggable

	IMAGE_SIZES = {
		:default 	=> [500, 500],
		:thumb 		=> [770, 420],
		:header 	=> [770, 420],
		:mini 		=> [80, 80]
	}

	validates_presence_of :title	
	validates_presence_of :slug

	scope :most_recent, -> {order('created_at DESC')}
	scope :publish, -> {order('created_at DESC').where(status: 'publish')}

	friendly_id :title, use: [:slugged, :history]	
end
