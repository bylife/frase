class Service < ActiveRecord::Base
	extend FriendlyId

	validates_presence_of :title	
	validates_presence_of :slug

	IMAGE_SIZES = {
		:default 	=> [1900, 280],
		:thumb 		=> [390, 390],
		:header 	=> [1901, 280],
		:mini 		=> [80, 80]
	}

	mount_uploader :image, PictureUploader
	mount_uploader :header_image, PictureUploader
	scope :most_recent, -> {order('created_at DESC')}
	default_scope { order('id ASC') }

	friendly_id :title, use: [:slugged, :history]

	def self.search(query)
		if query.present?
			where(['title LIKE :query OR body LIKE :query', query: '%#{query}%'])
		else
			Service.all.all
		end
	end
end
