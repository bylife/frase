class Customer < ActiveRecord::Base
	
	IMAGE_SIZES = {
		:default 	=> [500, 500],
		:thumb 		=> [170, 170],
		:header 	=> [1900, 280],
		:mini 		=> [80, 80]
	}
	default_scope { order('id ASC') }
	scope :type_services, -> { where(jobtype: 'services') }
	scope :type_courses, -> { where(jobtype: 'courses') }

	mount_uploader :image, PictureUploader
end
