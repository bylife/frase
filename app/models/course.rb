class Course < ActiveRecord::Base
	extend FriendlyId
	belongs_to :member
	belongs_to :modality
	has_many :classrooms
	
	IMAGE_SIZES = {
		:default 	=> [1900, 280],
		:header 	=> [1900, 280],
		:thumb 		=> [390, 390],
		:mini 		=> [80, 80]
	}
	mount_uploader :thumbnail, PictureUploader
	mount_uploader :header_image, PictureUploader

	default_scope { order('id ASC') }
	scope :modality, -> (modality) { where(modality_id: modality) }
	scope :presencial, -> { where(modality_id: 3) }

	validates_presence_of :title, :value, :workload, :member_id, :modality_id
	
	friendly_id :title, use: [:slugged, :history]	
end