class Job < ActiveRecord::Base
	extend FriendlyId
	IMAGE_SIZES = {
		:default 	=> [500, 500],
		:header 	=> [770, 485],
		:thumb 		=> [270, 170],
		:mini 		=> [80, 80]
	}
	mount_uploader :image, PictureUploader

	belongs_to :type

	validates_presence_of :title	
	validates_presence_of :type_id	
	validates_presence_of :slug

	scope :most_recent, -> {order('created_at DESC')}

	friendly_id :title, use: [:slugged, :history]	

	def self.search(query)
		if query.present?
			where(['title LIKE :query OR client LIKE :query', query: '%#{query}%'])
		else
			Job.all.all
		end
	end
end
