class Category < ActiveRecord::Base
	extend FriendlyId
	belongs_to 	:parent, :class_name => 'Category', :foreign_key => 'category_id'
	has_many 	:children, :class_name => 'Category', :foreign_key => 'category_id'
	has_many 	:posts

	validates_presence_of :title	
	validates_presence_of :slug

	friendly_id :title, use: [:slugged, :history]
end
