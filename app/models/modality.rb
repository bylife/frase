class Modality < ActiveRecord::Base
	extend FriendlyId
	has_many :courses
	friendly_id :title, use: [:slugged, :history]
	default_scope { order('id ASC') }
end
