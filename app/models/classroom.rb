class Classroom < ActiveRecord::Base
  belongs_to :course
  has_many :subscriptions
  scope :next_courses, -> { where(['open = true AND start > ?', DateTime.now]) }
  scope :opened, -> (course_id){ where(:open => true, :course_id => course_id) }
  
end
