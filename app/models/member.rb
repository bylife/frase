class Member < ActiveRecord::Base
	validates_presence_of :name, :image
	has_many :courses

	IMAGE_SIZES = {
		:default 	=> [500, 500],
		:header 	=> [1900, 440],
		:thumb 		=> [270, 270],
		:mini 		=> [80, 80]
	}
	mount_uploader :image, PictureUploader

	default_scope { order('id ASC') }
	scope :most_recent, -> {order('created_at DESC')}

	def self.get_team
		all = Hash.new
		Member.all.each_with_index do |m, i|
			if i%4==0
				all['a'] = m
			end
		end
		all
	end
end
