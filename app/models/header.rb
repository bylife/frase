class Header < ActiveRecord::Base
	IMAGE_SIZES = {
		:default 	=> [1900, 280],
		:header 	=> [1900, 280],
		:thumb 		=> [150, 150],
		:mini 		=> [80, 80]
	}
	mount_uploader :image, PictureUploader	
	default_scope { order('id ASC') }

end
