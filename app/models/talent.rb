class Talent < ActiveRecord::Base
	IMAGE_SIZES = {
		:default 	=> [500, 500],
		:header 	=> [500, 500],
		:thumb 		=> [163, 163],
		:mini 		=> [163, 163]
	}
	mount_uploader :photo, PictureUploader
end
