class User < ActiveRecord::Base
	has_many :post
	scope :most_recent, -> { order('created_at DESC') }
	scope :active, -> { where(active: true) }

	EMAIL_REGEXP = /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
	validates_presence_of :email, :full_name
	validates_format_of :email, with: EMAIL_REGEXP

	has_secure_password

	def self.authenticate(email, password)
		user = active.
							find_by(email: email).
							try(:authenticate, password)
	end

	def password=(password)
		self.password_digest = BCrypt::Password.create(password)
	end

	def is_password?(password)
		BCrypt::Password.new(self.password_digest) == password
	end
end
