class Type < ActiveRecord::Base
	extend FriendlyId

	has_many :jobs

	scope :most_recent, -> {order('created_at DESC')}

	friendly_id :title, use: [:slugged, :history]	

	def self.search(query)
		if query.present?
			where(['title LIKE :query OR description LIKE :query', query: '%#{query}%'])
		else
			Type.all.all
		end
	end
end
