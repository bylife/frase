json.array!(@classrooms) do |classroom|
  json.extract! classroom, :id, :title, :course, :course_slug, :start, :end, :hour, :city, :value, :installments, :discount
  json.url classroom_url(classroom, format: :json)
end
