json.extract! @classroom, :id, :title, :course, :course_slug, :start, :end, :city, :value, :installments, :discount, :created_at, :updated_at
