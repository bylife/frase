json.array!(@classrooms) do |classroom|
  json.extract! classroom, :id, :title, :course_id, :date, :city, :value, :discount
  json.url classroom_url(classroom, format: :json)
end
