json.array!(@testimonies) do |testimony|
  json.extract! testimony, :id, :text, :author
  json.url testimony_url(testimony, format: :json)
end
