json.array!(@services) do |service|
  json.extract! service, :id, :title, :body, :slug, :url, :picture_url, :thumb_url
  json.url service_url(service, format: :json)
end
