json.array!(@formats) do |format|
  json.extract! format, :id, :title, :image, :description
  json.url format_url(format, format: :json)
end
