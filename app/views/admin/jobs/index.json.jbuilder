json.array!(@jobs) do |job|
  json.extract! job, :id, :type_id, :type, :title, :thumb_url, :picture_url, :slug, :video, :client, :need, :solution, :result, :technical_form
  json.url job_url(job, format: :json)
end
