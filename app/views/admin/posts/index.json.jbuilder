json.array!(@posts) do |post|
  json.extract! post, :id, :title, :thumbnail, :body, :slug, :user_id, :status, :date
  json.url post_url(post, format: :json)
end
