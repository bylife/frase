json.array!(@courses) do |course|
  json.extract! course, :id, :title, :modality, :introduction, :thumbnail, :thumbnail_url, :header_image, :header_url, :slug, :purpose, :workload, :lessons, :didatics_resources, :feedback, :member, :about, :important_note, :value, :value_observations, :discount, :discount_observations
  json.url course_url(course, format: :json)
end
