json.array!(@customers) do |customer|
  json.extract! customer, :id, :title, :jobtype, :url, :image, :picture_url, :thumb_url
  json.url customer_url(customer, format: :json)
end
