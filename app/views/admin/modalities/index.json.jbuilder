json.array!(@modalities) do |modality|
  json.extract! modality, :id, :title, :about, :info
  json.url modality_url(modality, format: :json)
end
