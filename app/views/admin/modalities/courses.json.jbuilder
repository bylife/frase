json.array!(@modalities) do |modality|
  json.extract! modality, :title, :courses
  json.url modality_url(modality, format: :json)
end
