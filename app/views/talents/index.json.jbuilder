json.array!(@talents) do |talent|
  json.extract! talent, :id, :name, :photo, :linkedin
  json.url talent_url(talent, format: :json)
end
