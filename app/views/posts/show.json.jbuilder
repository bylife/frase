json.extract! @post, :id, :title, :thumbnail, :thumb_url, :picture_url, :tag_list, :category, :body, :the_excerpt, :the_content, :slug, :username, :status, :date, :created_at, :updated_at
