json.array!(@posts) do |post|
  json.extract! post, :id, :title, :thumbnail, :thumb_url, :picture_url, :tag_list, :body, :the_excerpt, :the_content, :category, :slug, :username, :status, :date, :created_at, :updated_at
  json.url post_url(post, format: :json)
end
