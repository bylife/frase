json.array!(@courses) do |course|
  json.extract! course, :id, :title, :modality, :slug
  json.url course_url(course, format: :json)
end
