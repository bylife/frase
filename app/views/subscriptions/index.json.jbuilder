json.array!(@subscriptions) do |subscription|
  json.extract! subscription, :id, :classroom_id, :student_id
  json.url subscription_url(subscription, format: :json)
end
