json.array!(@modalities) do |modality|
  json.extract! modality, :title, :courses, :slug
  json.url modality_url(modality, format: :json)
end
