json.array!(@videos) do |video|
  json.extract! video, :id, :title, :video_file, :page
  json.url video_url(video, format: :json)
end
