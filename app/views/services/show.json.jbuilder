json.extract! @service, :id, :title, :url, :body, :slug, :picture_url, :header_image_url, :header_image, :thumb_url, :created_at, :updated_at
