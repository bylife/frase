json.array!(@members) do |member|
  json.extract! member, :id, :name, :image, :thumb_url, :picture_url, :profile, :raw_profile
  json.url member_url(member, format: :json)
end
