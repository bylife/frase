json.array!(@students) do |student|
  json.extract! student, :id, :name, :nickname, :rg, :cpf, :birth, :gender, :occupation, :email, :phone, :comercial_phone, :zipcode, :address, :number, :complement, :neighborhood, :city, :state, :billing_type
  json.url student_url(student, format: :json)
end
