json.extract! @job, :id, :title, :type_id, :type, :header_url, :thumb_url, :picture_url, :slug, :video, :client, :need, :solution, :result, :technical_form, :created_at, :updated_at
