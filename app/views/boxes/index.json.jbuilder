json.array!(@boxes) do |box|
  json.extract! box, :id, :title, :body, :image, :video, :page
  json.url box_url(box, format: :json)
end
