json.array!(@vouchers) do |voucher|
  json.extract! voucher, :id, :code, :classroom_id, :email, :cpf, :observations
  json.url voucher_url(voucher, format: :json)
end
