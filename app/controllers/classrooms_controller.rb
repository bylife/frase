class ClassroomsController < ApplicationController
  before_action :set_classroom, only: [:show, :edit, :update, :destroy]
  #before_action :require_authentication, only: [:create, :new, :edit, :update, :destroy]

  # GET /classrooms
  # GET /classrooms.json
  def index
    classrooms = Classroom.all
    @classrooms = classrooms.all.map do |classroom|
      ClassroomPresenter.new(classroom)
    end
  end

  def nextcourses
    next_classrooms = Classroom.next_courses
    @classrooms = next_classrooms.all.map do |nc|
      ClassroomPresenter.new(nc)
    end
    render :index
  end

  def opened
    classrooms = Classroom.opened(params[:course])
    @classrooms = classrooms.all.map do |classroom|
      ClassroomPresenter.new(classroom)
    end
    render :index
  end

  # GET /classrooms/1
  # GET /classrooms/1.json
  def show    
    classroom = Classroom.find(params[:id])
    @classroom = ClassroomPresenter.new(classroom)
  end

  # GET /classrooms/new
  def new
    @classroom = Classroom.new
  end

  # GET /classrooms/1/edit
  def edit
  end

  # POST /classrooms
  # POST /classrooms.json
  def create
    @classroom = Classroom.new(classroom_params)

    respond_to do |format|
      if @classroom.save
        format.html { redirect_to @classroom, notice: 'Classroom was successfully created.' }
        format.json { render :show, status: :created, location: @classroom }
      else
        format.html { render :new }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /classrooms/1
  # PATCH/PUT /classrooms/1.json
  def update
    respond_to do |format|
      if @classroom.update(classroom_params)
        format.html { redirect_to @classroom, notice: 'Classroom was successfully updated.' }
        format.json { render :show, status: :ok, location: @classroom }
      else
        format.html { render :edit }
        format.json { render json: @classroom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classrooms/1
  # DELETE /classrooms/1.json
  def destroy
    @classroom.destroy
    respond_to do |format|
      format.html { redirect_to classrooms_url, notice: 'Classroom was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_classroom
      @classroom = Classroom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def classroom_params
      params.require(:classroom).permit(:title, :course_id, :start, :end, :place, :hour, :open, :city, :value, :discount)
    end
end
