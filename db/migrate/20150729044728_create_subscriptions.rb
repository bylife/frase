class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :classroom, index: true
      t.references :student, index: true

      t.timestamps
    end
  end
end
