class AddModalityIdToCourses < ActiveRecord::Migration
  def change
    add_reference :courses, :modality, index: true
  end
end
