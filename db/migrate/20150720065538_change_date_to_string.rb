class ChangeDateToString < ActiveRecord::Migration
  def change
  	change_column :classrooms, :date, :text
  end
end
