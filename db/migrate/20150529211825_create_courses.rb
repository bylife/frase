class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :title
      t.string :thumbnail
      t.text :purpose
      t.string :workload
      t.string :lessons
      t.string :didatics_resources
      t.string :feedback
      t.integer :member_id
      t.text :about
      t.text :important_note
      t.float :value
      t.string :value_observations
      t.float :discount
      t.string :discount_observations

      t.timestamps
    end
  end
end
