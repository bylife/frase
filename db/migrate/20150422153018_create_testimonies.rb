class CreateTestimonies < ActiveRecord::Migration
  def change
    create_table :testimonies do |t|
      t.string :text
      t.string :author

      t.timestamps
    end
  end
end
