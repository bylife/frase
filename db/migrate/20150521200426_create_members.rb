class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :name
      t.string :image
      t.text :profile

      t.timestamps
    end
  end
end
