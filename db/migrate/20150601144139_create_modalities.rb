class CreateModalities < ActiveRecord::Migration
  def change
    create_table :modalities do |t|
      t.string :title
      t.text :about
      t.text :info

      t.timestamps
    end
  end
end
