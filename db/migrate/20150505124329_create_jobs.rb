class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :client
      t.string :author
      t.string :solution
      t.string :result
      t.string :credits

      t.timestamps
    end
  end
end
