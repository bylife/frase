class CreateBoxes < ActiveRecord::Migration
  def change
    create_table :boxes do |t|
      t.string :title
      t.text :body
      t.string :image
      t.string :video
      t.string :page

      t.timestamps
    end
  end
end
