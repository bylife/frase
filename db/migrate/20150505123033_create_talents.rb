class CreateTalents < ActiveRecord::Migration
  def change
    create_table :talents do |t|
      t.string :name
      t.string :photo
      t.string :linkedin

      t.timestamps
    end
  end
end
