class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.string :code
      t.references :classroom, index: true
      t.string :email
      t.string :cpf
      t.text :observations

      t.timestamps
    end
  end
end
