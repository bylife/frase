class RemoveIndexServicesOnSlug < ActiveRecord::Migration
  def change
  	remove_index :services, :slug
  end
end
