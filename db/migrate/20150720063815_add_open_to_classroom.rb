class AddOpenToClassroom < ActiveRecord::Migration
  def change
    add_column :classrooms, :open, :boolean
  end
end
