class AddHeaderImageToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :header_image, :string
  end
end
