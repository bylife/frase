class AddImageAndVideoToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :image, :string
    add_column :jobs, :video, :string
  end
end
