class ChangeAuthorJobs < ActiveRecord::Migration
  def change
  	rename_column :jobs, :author, :need
  	rename_column :jobs, :credits, :technical_form
  end
end
