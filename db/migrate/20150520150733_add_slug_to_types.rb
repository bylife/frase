class AddSlugToTypes < ActiveRecord::Migration
  def change
    add_column :types, :slug, :string
  end
end
