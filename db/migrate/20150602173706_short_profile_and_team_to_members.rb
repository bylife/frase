class ShortProfileAndTeamToMembers < ActiveRecord::Migration
  def change
  	add_column :members, :short_profile, :text
  	add_column :members, :team, :boolean
  end
end
