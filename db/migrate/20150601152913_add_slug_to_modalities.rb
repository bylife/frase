class AddSlugToModalities < ActiveRecord::Migration
  def change
    add_column :modalities, :slug, :string
  end
end
