class AddInfoToTestimonies < ActiveRecord::Migration
  def change
    add_column :testimonies, :info, :string
  end
end
