class ChangeSizesJobs < ActiveRecord::Migration
  def change
  	change_column :jobs, :need, :text
  	change_column :jobs, :solution, :text
  	change_column :jobs, :result, :text
  	change_column :jobs, :technical_form, :text
  end
end
