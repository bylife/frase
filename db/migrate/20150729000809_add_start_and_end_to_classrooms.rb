class AddStartAndEndToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :start, :datetime
    add_column :classrooms, :end, :datetime
    remove_column :classrooms, :date
  end
end
