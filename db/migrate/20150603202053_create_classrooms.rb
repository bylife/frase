class CreateClassrooms < ActiveRecord::Migration
  def change
    create_table :classrooms do |t|
      t.string :title
      t.references :course, index: true
      t.date :date
      t.string :city
      t.float :value
      t.float :discount

      t.timestamps
    end
  end
end
