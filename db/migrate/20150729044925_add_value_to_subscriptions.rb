class AddValueToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :value, :float
  end
end
