class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :title
      t.string :url
      t.string :image

      t.timestamps
    end
  end
end
