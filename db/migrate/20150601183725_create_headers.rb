class CreateHeaders < ActiveRecord::Migration
  def change
    create_table :headers do |t|
      t.string :title
      t.text :description
      t.string :image
      t.string :page
      t.string :link

      t.timestamps
    end
  end
end
