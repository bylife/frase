class AddHourToClassroom < ActiveRecord::Migration
  def change
    add_column :classrooms, :hour, :string
  end
end
