class AddPagseguroidToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :pagseguro_id, :string
  end
end
