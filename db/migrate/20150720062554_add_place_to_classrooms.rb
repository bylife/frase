class AddPlaceToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :place, :string
  end
end
