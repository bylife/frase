class AddHeaderImageToServices < ActiveRecord::Migration
  def change
    add_column :services, :header_image, :string
  end
end
