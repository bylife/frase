class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name
      t.string :nickname
      t.string :rg
      t.string :cpf
      t.date :birth
      t.string :gender
      t.string :occupation
      t.string :email
      t.string :phone
      t.string :comercial_phone
      t.string :zipcode
      t.string :address
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :city
      t.string :state
      t.integer :billing_type

      t.timestamps
    end
  end
end
