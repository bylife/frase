# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150729151604) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boxes", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.string   "image"
    t.string   "video"
    t.string   "page"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "classrooms", force: true do |t|
    t.string   "title"
    t.integer  "course_id"
    t.string   "city"
    t.float    "value"
    t.float    "discount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "place"
    t.string   "hour"
    t.boolean  "open"
    t.datetime "start"
    t.datetime "end"
  end

  add_index "classrooms", ["course_id"], name: "index_classrooms_on_course_id", using: :btree

  create_table "courses", force: true do |t|
    t.string   "title"
    t.string   "thumbnail"
    t.text     "purpose"
    t.string   "workload"
    t.string   "lessons"
    t.string   "didatics_resources"
    t.string   "feedback"
    t.integer  "member_id"
    t.text     "about"
    t.text     "important_note"
    t.float    "value"
    t.string   "value_observations"
    t.float    "discount"
    t.string   "discount_observations"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "modality_id"
    t.string   "header_image"
    t.text     "introduction"
  end

  add_index "courses", ["modality_id"], name: "index_courses_on_modality_id", using: :btree

  create_table "customers", force: true do |t|
    t.string   "title"
    t.string   "url"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "jobtype"
  end

  create_table "formats", force: true do |t|
    t.string   "title"
    t.string   "image"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "headers", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.string   "page"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.string   "title"
    t.string   "client"
    t.text     "need"
    t.text     "solution"
    t.text     "result"
    t.text     "technical_form"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "image"
    t.string   "video"
    t.integer  "type_id"
  end

  create_table "members", force: true do |t|
    t.string   "name"
    t.string   "image"
    t.text     "profile"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "short_profile"
    t.boolean  "team"
  end

  create_table "modalities", force: true do |t|
    t.string   "title"
    t.text     "about"
    t.text     "info"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "posts", force: true do |t|
    t.string   "title"
    t.string   "thumbnail"
    t.text     "body"
    t.string   "slug"
    t.integer  "user_id"
    t.string   "status"
    t.string   "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
  end

  create_table "redactor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], name: "idx_redactor_assetable", using: :btree
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_redactor_assetable_type", using: :btree

  create_table "rich_rich_files", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "rich_file_file_name"
    t.string   "rich_file_content_type"
    t.integer  "rich_file_file_size"
    t.datetime "rich_file_updated_at"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.text     "uri_cache"
    t.string   "simplified_type",        default: "file"
  end

  create_table "services", force: true do |t|
    t.string   "title"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.text     "body"
    t.string   "header_image"
  end

  create_table "students", force: true do |t|
    t.string   "name"
    t.string   "nickname"
    t.string   "rg"
    t.string   "cpf"
    t.date     "birth"
    t.string   "gender"
    t.string   "occupation"
    t.string   "email"
    t.string   "phone"
    t.string   "comercial_phone"
    t.string   "zipcode"
    t.string   "address"
    t.string   "number"
    t.string   "complement"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "state"
    t.integer  "billing_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscriptions", force: true do |t|
    t.integer  "classroom_id"
    t.integer  "student_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "value"
    t.string   "pagseguro_id"
  end

  add_index "subscriptions", ["classroom_id"], name: "index_subscriptions_on_classroom_id", using: :btree
  add_index "subscriptions", ["student_id"], name: "index_subscriptions_on_student_id", using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "talents", force: true do |t|
    t.string   "name"
    t.string   "photo"
    t.string   "linkedin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "testimonies", force: true do |t|
    t.text     "text"
    t.string   "author"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "info"
  end

  create_table "types", force: true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  create_table "users", force: true do |t|
    t.string   "full_name"
    t.string   "email"
    t.string   "password_digest"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "videos", force: true do |t|
    t.string   "title"
    t.string   "video_file"
    t.string   "page"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vouchers", force: true do |t|
    t.string   "code"
    t.integer  "classroom_id"
    t.string   "email"
    t.string   "cpf"
    t.text     "observations"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "vouchers", ["classroom_id"], name: "index_vouchers_on_classroom_id", using: :btree

end
