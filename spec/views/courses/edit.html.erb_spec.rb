require 'rails_helper'

RSpec.describe "courses/edit", type: :view do
  before(:each) do
    @course = assign(:course, Course.create!(
      :title => "MyString",
      :thumbnail => "MyString",
      :purpose => "MyText",
      :workload => "MyString",
      :lessons => "MyString",
      :didatics_resources => "MyString",
      :feedback => "MyString",
      :member_id => "",
      :about => "MyText",
      :important_note => "MyText",
      :value => 1.5,
      :value_observations => "MyString",
      :discount => 1.5,
      :discount_observations => "MyString"
    ))
  end

  it "renders the edit course form" do
    render

    assert_select "form[action=?][method=?]", course_path(@course), "post" do

      assert_select "input#course_title[name=?]", "course[title]"

      assert_select "input#course_thumbnail[name=?]", "course[thumbnail]"

      assert_select "textarea#course_purpose[name=?]", "course[purpose]"

      assert_select "input#course_workload[name=?]", "course[workload]"

      assert_select "input#course_lessons[name=?]", "course[lessons]"

      assert_select "input#course_didatics_resources[name=?]", "course[didatics_resources]"

      assert_select "input#course_feedback[name=?]", "course[feedback]"

      assert_select "input#course_member_id[name=?]", "course[member_id]"

      assert_select "textarea#course_about[name=?]", "course[about]"

      assert_select "textarea#course_important_note[name=?]", "course[important_note]"

      assert_select "input#course_value[name=?]", "course[value]"

      assert_select "input#course_value_observations[name=?]", "course[value_observations]"

      assert_select "input#course_discount[name=?]", "course[discount]"

      assert_select "input#course_discount_observations[name=?]", "course[discount_observations]"
    end
  end
end
