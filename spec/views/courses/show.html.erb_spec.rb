require 'rails_helper'

RSpec.describe "courses/show", type: :view do
  before(:each) do
    @course = assign(:course, Course.create!(
      :title => "Title",
      :thumbnail => "Thumbnail",
      :purpose => "MyText",
      :workload => "Workload",
      :lessons => "Lessons",
      :didatics_resources => "Didatics Resources",
      :feedback => "Feedback",
      :member_id => "",
      :about => "MyText",
      :important_note => "MyText",
      :value => 1.5,
      :value_observations => "Value Observations",
      :discount => 1.5,
      :discount_observations => "Discount Observations"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Thumbnail/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Workload/)
    expect(rendered).to match(/Lessons/)
    expect(rendered).to match(/Didatics Resources/)
    expect(rendered).to match(/Feedback/)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/Value Observations/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/Discount Observations/)
  end
end
