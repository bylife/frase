require 'rails_helper'

RSpec.describe "courses/index", type: :view do
  before(:each) do
    assign(:courses, [
      Course.create!(
        :title => "Title",
        :thumbnail => "Thumbnail",
        :purpose => "MyText",
        :workload => "Workload",
        :lessons => "Lessons",
        :didatics_resources => "Didatics Resources",
        :feedback => "Feedback",
        :member_id => "",
        :about => "MyText",
        :important_note => "MyText",
        :value => 1.5,
        :value_observations => "Value Observations",
        :discount => 1.5,
        :discount_observations => "Discount Observations"
      ),
      Course.create!(
        :title => "Title",
        :thumbnail => "Thumbnail",
        :purpose => "MyText",
        :workload => "Workload",
        :lessons => "Lessons",
        :didatics_resources => "Didatics Resources",
        :feedback => "Feedback",
        :member_id => "",
        :about => "MyText",
        :important_note => "MyText",
        :value => 1.5,
        :value_observations => "Value Observations",
        :discount => 1.5,
        :discount_observations => "Discount Observations"
      )
    ])
  end

  it "renders a list of courses" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Thumbnail".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Workload".to_s, :count => 2
    assert_select "tr>td", :text => "Lessons".to_s, :count => 2
    assert_select "tr>td", :text => "Didatics Resources".to_s, :count => 2
    assert_select "tr>td", :text => "Feedback".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Value Observations".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Discount Observations".to_s, :count => 2
  end
end
