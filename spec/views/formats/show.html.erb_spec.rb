require 'rails_helper'

RSpec.describe "formats/show", type: :view do
  before(:each) do
    @format = assign(:format, Format.create!(
      :title => "Title",
      :image => "Image",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Image/)
    expect(rendered).to match(/MyText/)
  end
end
