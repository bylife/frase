require 'rails_helper'

RSpec.describe "formats/index", type: :view do
  before(:each) do
    assign(:formats, [
      Format.create!(
        :title => "Title",
        :image => "Image",
        :description => "MyText"
      ),
      Format.create!(
        :title => "Title",
        :image => "Image",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of formats" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
