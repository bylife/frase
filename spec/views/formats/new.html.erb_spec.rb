require 'rails_helper'

RSpec.describe "formats/new", type: :view do
  before(:each) do
    assign(:format, Format.new(
      :title => "MyString",
      :image => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new format form" do
    render

    assert_select "form[action=?][method=?]", formats_path, "post" do

      assert_select "input#format_title[name=?]", "format[title]"

      assert_select "input#format_image[name=?]", "format[image]"

      assert_select "textarea#format_description[name=?]", "format[description]"
    end
  end
end
