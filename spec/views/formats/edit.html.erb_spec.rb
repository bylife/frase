require 'rails_helper'

RSpec.describe "formats/edit", type: :view do
  before(:each) do
    @format = assign(:format, Format.create!(
      :title => "MyString",
      :image => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit format form" do
    render

    assert_select "form[action=?][method=?]", format_path(@format), "post" do

      assert_select "input#format_title[name=?]", "format[title]"

      assert_select "input#format_image[name=?]", "format[image]"

      assert_select "textarea#format_description[name=?]", "format[description]"
    end
  end
end
