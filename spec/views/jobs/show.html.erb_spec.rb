require 'rails_helper'

RSpec.describe "jobs/show", type: :view do
  before(:each) do
    @job = assign(:job, Job.create!(
      :title => "Title",
      :client => "Client",
      :author => "Author",
      :solution => "Solution",
      :result => "Result",
      :credits => "Credits"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Client/)
    expect(rendered).to match(/Author/)
    expect(rendered).to match(/Solution/)
    expect(rendered).to match(/Result/)
    expect(rendered).to match(/Credits/)
  end
end
