require 'rails_helper'

RSpec.describe "jobs/index", type: :view do
  before(:each) do
    assign(:jobs, [
      Job.create!(
        :title => "Title",
        :client => "Client",
        :author => "Author",
        :solution => "Solution",
        :result => "Result",
        :credits => "Credits"
      ),
      Job.create!(
        :title => "Title",
        :client => "Client",
        :author => "Author",
        :solution => "Solution",
        :result => "Result",
        :credits => "Credits"
      )
    ])
  end

  it "renders a list of jobs" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Client".to_s, :count => 2
    assert_select "tr>td", :text => "Author".to_s, :count => 2
    assert_select "tr>td", :text => "Solution".to_s, :count => 2
    assert_select "tr>td", :text => "Result".to_s, :count => 2
    assert_select "tr>td", :text => "Credits".to_s, :count => 2
  end
end
