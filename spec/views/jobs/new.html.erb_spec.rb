require 'rails_helper'

RSpec.describe "jobs/new", type: :view do
  before(:each) do
    assign(:job, Job.new(
      :title => "MyString",
      :client => "MyString",
      :author => "MyString",
      :solution => "MyString",
      :result => "MyString",
      :credits => "MyString"
    ))
  end

  it "renders new job form" do
    render

    assert_select "form[action=?][method=?]", jobs_path, "post" do

      assert_select "input#job_title[name=?]", "job[title]"

      assert_select "input#job_client[name=?]", "job[client]"

      assert_select "input#job_author[name=?]", "job[author]"

      assert_select "input#job_solution[name=?]", "job[solution]"

      assert_select "input#job_result[name=?]", "job[result]"

      assert_select "input#job_credits[name=?]", "job[credits]"
    end
  end
end
