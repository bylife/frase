require 'rails_helper'

RSpec.describe "jobs/edit", type: :view do
  before(:each) do
    @job = assign(:job, Job.create!(
      :title => "MyString",
      :client => "MyString",
      :author => "MyString",
      :solution => "MyString",
      :result => "MyString",
      :credits => "MyString"
    ))
  end

  it "renders the edit job form" do
    render

    assert_select "form[action=?][method=?]", job_path(@job), "post" do

      assert_select "input#job_title[name=?]", "job[title]"

      assert_select "input#job_client[name=?]", "job[client]"

      assert_select "input#job_author[name=?]", "job[author]"

      assert_select "input#job_solution[name=?]", "job[solution]"

      assert_select "input#job_result[name=?]", "job[result]"

      assert_select "input#job_credits[name=?]", "job[credits]"
    end
  end
end
