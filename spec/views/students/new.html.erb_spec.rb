require 'rails_helper'

RSpec.describe "students/new", type: :view do
  before(:each) do
    assign(:student, Student.new(
      :name => "MyString",
      :nickname => "MyString",
      :rg => "MyString",
      :cpf => "MyString",
      :gender => "MyString",
      :occupation => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :comercial_phone => "MyString",
      :zipcode => "MyString",
      :address => "MyString",
      :number => "MyString",
      :complement => "MyString",
      :neighborhood => "MyString",
      :city => "MyString",
      :state => "MyString",
      :billing_type => 1
    ))
  end

  it "renders new student form" do
    render

    assert_select "form[action=?][method=?]", students_path, "post" do

      assert_select "input#student_name[name=?]", "student[name]"

      assert_select "input#student_nickname[name=?]", "student[nickname]"

      assert_select "input#student_rg[name=?]", "student[rg]"

      assert_select "input#student_cpf[name=?]", "student[cpf]"

      assert_select "input#student_gender[name=?]", "student[gender]"

      assert_select "input#student_occupation[name=?]", "student[occupation]"

      assert_select "input#student_email[name=?]", "student[email]"

      assert_select "input#student_phone[name=?]", "student[phone]"

      assert_select "input#student_comercial_phone[name=?]", "student[comercial_phone]"

      assert_select "input#student_zipcode[name=?]", "student[zipcode]"

      assert_select "input#student_address[name=?]", "student[address]"

      assert_select "input#student_number[name=?]", "student[number]"

      assert_select "input#student_complement[name=?]", "student[complement]"

      assert_select "input#student_neighborhood[name=?]", "student[neighborhood]"

      assert_select "input#student_city[name=?]", "student[city]"

      assert_select "input#student_state[name=?]", "student[state]"

      assert_select "input#student_billing_type[name=?]", "student[billing_type]"
    end
  end
end
