require 'rails_helper'

RSpec.describe "students/index", type: :view do
  before(:each) do
    assign(:students, [
      Student.create!(
        :name => "Name",
        :nickname => "Nickname",
        :rg => "Rg",
        :cpf => "Cpf",
        :gender => "Gender",
        :occupation => "Occupation",
        :email => "Email",
        :phone => "Phone",
        :comercial_phone => "Comercial Phone",
        :zipcode => "Zipcode",
        :address => "Address",
        :number => "Number",
        :complement => "Complement",
        :neighborhood => "Neighborhood",
        :city => "City",
        :state => "State",
        :billing_type => 1
      ),
      Student.create!(
        :name => "Name",
        :nickname => "Nickname",
        :rg => "Rg",
        :cpf => "Cpf",
        :gender => "Gender",
        :occupation => "Occupation",
        :email => "Email",
        :phone => "Phone",
        :comercial_phone => "Comercial Phone",
        :zipcode => "Zipcode",
        :address => "Address",
        :number => "Number",
        :complement => "Complement",
        :neighborhood => "Neighborhood",
        :city => "City",
        :state => "State",
        :billing_type => 1
      )
    ])
  end

  it "renders a list of students" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Nickname".to_s, :count => 2
    assert_select "tr>td", :text => "Rg".to_s, :count => 2
    assert_select "tr>td", :text => "Cpf".to_s, :count => 2
    assert_select "tr>td", :text => "Gender".to_s, :count => 2
    assert_select "tr>td", :text => "Occupation".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Comercial Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Zipcode".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Number".to_s, :count => 2
    assert_select "tr>td", :text => "Complement".to_s, :count => 2
    assert_select "tr>td", :text => "Neighborhood".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
