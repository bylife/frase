require 'rails_helper'

RSpec.describe "students/show", type: :view do
  before(:each) do
    @student = assign(:student, Student.create!(
      :name => "Name",
      :nickname => "Nickname",
      :rg => "Rg",
      :cpf => "Cpf",
      :gender => "Gender",
      :occupation => "Occupation",
      :email => "Email",
      :phone => "Phone",
      :comercial_phone => "Comercial Phone",
      :zipcode => "Zipcode",
      :address => "Address",
      :number => "Number",
      :complement => "Complement",
      :neighborhood => "Neighborhood",
      :city => "City",
      :state => "State",
      :billing_type => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Nickname/)
    expect(rendered).to match(/Rg/)
    expect(rendered).to match(/Cpf/)
    expect(rendered).to match(/Gender/)
    expect(rendered).to match(/Occupation/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Comercial Phone/)
    expect(rendered).to match(/Zipcode/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Number/)
    expect(rendered).to match(/Complement/)
    expect(rendered).to match(/Neighborhood/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/1/)
  end
end
