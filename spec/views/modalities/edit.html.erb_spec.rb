require 'rails_helper'

RSpec.describe "modalities/edit", type: :view do
  before(:each) do
    @modality = assign(:modality, Modality.create!(
      :title => "MyString",
      :about => "MyText",
      :info => "MyText"
    ))
  end

  it "renders the edit modality form" do
    render

    assert_select "form[action=?][method=?]", modality_path(@modality), "post" do

      assert_select "input#modality_title[name=?]", "modality[title]"

      assert_select "textarea#modality_about[name=?]", "modality[about]"

      assert_select "textarea#modality_info[name=?]", "modality[info]"
    end
  end
end
