require 'rails_helper'

RSpec.describe "modalities/index", type: :view do
  before(:each) do
    assign(:modalities, [
      Modality.create!(
        :title => "Title",
        :about => "MyText",
        :info => "MyText"
      ),
      Modality.create!(
        :title => "Title",
        :about => "MyText",
        :info => "MyText"
      )
    ])
  end

  it "renders a list of modalities" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
