require 'rails_helper'

RSpec.describe "modalities/new", type: :view do
  before(:each) do
    assign(:modality, Modality.new(
      :title => "MyString",
      :about => "MyText",
      :info => "MyText"
    ))
  end

  it "renders new modality form" do
    render

    assert_select "form[action=?][method=?]", modalities_path, "post" do

      assert_select "input#modality_title[name=?]", "modality[title]"

      assert_select "textarea#modality_about[name=?]", "modality[about]"

      assert_select "textarea#modality_info[name=?]", "modality[info]"
    end
  end
end
