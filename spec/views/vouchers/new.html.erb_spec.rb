require 'rails_helper'

RSpec.describe "vouchers/new", type: :view do
  before(:each) do
    assign(:voucher, Voucher.new(
      :code => "MyString",
      :classroom => nil,
      :email => "MyString",
      :cpf => "MyString",
      :observations => "MyText"
    ))
  end

  it "renders new voucher form" do
    render

    assert_select "form[action=?][method=?]", vouchers_path, "post" do

      assert_select "input#voucher_code[name=?]", "voucher[code]"

      assert_select "input#voucher_classroom_id[name=?]", "voucher[classroom_id]"

      assert_select "input#voucher_email[name=?]", "voucher[email]"

      assert_select "input#voucher_cpf[name=?]", "voucher[cpf]"

      assert_select "textarea#voucher_observations[name=?]", "voucher[observations]"
    end
  end
end
