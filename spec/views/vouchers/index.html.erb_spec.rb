require 'rails_helper'

RSpec.describe "vouchers/index", type: :view do
  before(:each) do
    assign(:vouchers, [
      Voucher.create!(
        :code => "Code",
        :classroom => nil,
        :email => "Email",
        :cpf => "Cpf",
        :observations => "MyText"
      ),
      Voucher.create!(
        :code => "Code",
        :classroom => nil,
        :email => "Email",
        :cpf => "Cpf",
        :observations => "MyText"
      )
    ])
  end

  it "renders a list of vouchers" do
    render
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Cpf".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
