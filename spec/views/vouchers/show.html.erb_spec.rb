require 'rails_helper'

RSpec.describe "vouchers/show", type: :view do
  before(:each) do
    @voucher = assign(:voucher, Voucher.create!(
      :code => "Code",
      :classroom => nil,
      :email => "Email",
      :cpf => "Cpf",
      :observations => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Code/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Cpf/)
    expect(rendered).to match(/MyText/)
  end
end
