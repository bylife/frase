require 'rails_helper'

RSpec.describe "vouchers/edit", type: :view do
  before(:each) do
    @voucher = assign(:voucher, Voucher.create!(
      :code => "MyString",
      :classroom => nil,
      :email => "MyString",
      :cpf => "MyString",
      :observations => "MyText"
    ))
  end

  it "renders the edit voucher form" do
    render

    assert_select "form[action=?][method=?]", voucher_path(@voucher), "post" do

      assert_select "input#voucher_code[name=?]", "voucher[code]"

      assert_select "input#voucher_classroom_id[name=?]", "voucher[classroom_id]"

      assert_select "input#voucher_email[name=?]", "voucher[email]"

      assert_select "input#voucher_cpf[name=?]", "voucher[cpf]"

      assert_select "textarea#voucher_observations[name=?]", "voucher[observations]"
    end
  end
end
