require 'rails_helper'

RSpec.describe "headers/edit", type: :view do
  before(:each) do
    @header = assign(:header, Header.create!(
      :title => "MyString",
      :description => "MyText",
      :image => "MyString",
      :page => "MyString",
      :link => "MyString"
    ))
  end

  it "renders the edit header form" do
    render

    assert_select "form[action=?][method=?]", header_path(@header), "post" do

      assert_select "input#header_title[name=?]", "header[title]"

      assert_select "textarea#header_description[name=?]", "header[description]"

      assert_select "input#header_image[name=?]", "header[image]"

      assert_select "input#header_page[name=?]", "header[page]"

      assert_select "input#header_link[name=?]", "header[link]"
    end
  end
end
