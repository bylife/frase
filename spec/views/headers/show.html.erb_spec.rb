require 'rails_helper'

RSpec.describe "headers/show", type: :view do
  before(:each) do
    @header = assign(:header, Header.create!(
      :title => "Title",
      :description => "MyText",
      :image => "Image",
      :page => "Page",
      :link => "Link"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Image/)
    expect(rendered).to match(/Page/)
    expect(rendered).to match(/Link/)
  end
end
