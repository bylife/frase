require 'rails_helper'

RSpec.describe "headers/new", type: :view do
  before(:each) do
    assign(:header, Header.new(
      :title => "MyString",
      :description => "MyText",
      :image => "MyString",
      :page => "MyString",
      :link => "MyString"
    ))
  end

  it "renders new header form" do
    render

    assert_select "form[action=?][method=?]", headers_path, "post" do

      assert_select "input#header_title[name=?]", "header[title]"

      assert_select "textarea#header_description[name=?]", "header[description]"

      assert_select "input#header_image[name=?]", "header[image]"

      assert_select "input#header_page[name=?]", "header[page]"

      assert_select "input#header_link[name=?]", "header[link]"
    end
  end
end
