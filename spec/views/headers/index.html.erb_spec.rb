require 'rails_helper'

RSpec.describe "headers/index", type: :view do
  before(:each) do
    assign(:headers, [
      Header.create!(
        :title => "Title",
        :description => "MyText",
        :image => "Image",
        :page => "Page",
        :link => "Link"
      ),
      Header.create!(
        :title => "Title",
        :description => "MyText",
        :image => "Image",
        :page => "Page",
        :link => "Link"
      )
    ])
  end

  it "renders a list of headers" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
    assert_select "tr>td", :text => "Page".to_s, :count => 2
    assert_select "tr>td", :text => "Link".to_s, :count => 2
  end
end
