require 'rails_helper'

RSpec.describe "types/index", type: :view do
  before(:each) do
    assign(:types, [
      Type.create!(
        :title => "Title",
        :description => "Description"
      ),
      Type.create!(
        :title => "Title",
        :description => "Description"
      )
    ])
  end

  it "renders a list of types" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
