require 'rails_helper'

RSpec.describe "subscriptions/new", type: :view do
  before(:each) do
    assign(:subscription, Subscription.new(
      :classroom => nil,
      :student => nil
    ))
  end

  it "renders new subscription form" do
    render

    assert_select "form[action=?][method=?]", subscriptions_path, "post" do

      assert_select "input#subscription_classroom_id[name=?]", "subscription[classroom_id]"

      assert_select "input#subscription_student_id[name=?]", "subscription[student_id]"
    end
  end
end
