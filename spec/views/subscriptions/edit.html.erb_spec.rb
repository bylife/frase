require 'rails_helper'

RSpec.describe "subscriptions/edit", type: :view do
  before(:each) do
    @subscription = assign(:subscription, Subscription.create!(
      :classroom => nil,
      :student => nil
    ))
  end

  it "renders the edit subscription form" do
    render

    assert_select "form[action=?][method=?]", subscription_path(@subscription), "post" do

      assert_select "input#subscription_classroom_id[name=?]", "subscription[classroom_id]"

      assert_select "input#subscription_student_id[name=?]", "subscription[student_id]"
    end
  end
end
