require 'rails_helper'

RSpec.describe "customers/edit", type: :view do
  before(:each) do
    @customer = assign(:customer, Customer.create!(
      :title => "MyString",
      :url => "MyString",
      :image => "MyString"
    ))
  end

  it "renders the edit customer form" do
    render

    assert_select "form[action=?][method=?]", customer_path(@customer), "post" do

      assert_select "input#customer_title[name=?]", "customer[title]"

      assert_select "input#customer_url[name=?]", "customer[url]"

      assert_select "input#customer_image[name=?]", "customer[image]"
    end
  end
end
