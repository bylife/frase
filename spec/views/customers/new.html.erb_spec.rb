require 'rails_helper'

RSpec.describe "customers/new", type: :view do
  before(:each) do
    assign(:customer, Customer.new(
      :title => "MyString",
      :url => "MyString",
      :image => "MyString"
    ))
  end

  it "renders new customer form" do
    render

    assert_select "form[action=?][method=?]", customers_path, "post" do

      assert_select "input#customer_title[name=?]", "customer[title]"

      assert_select "input#customer_url[name=?]", "customer[url]"

      assert_select "input#customer_image[name=?]", "customer[image]"
    end
  end
end
