require 'rails_helper'

RSpec.describe "members/index", type: :view do
  before(:each) do
    assign(:members, [
      Member.create!(
        :name => "",
        :image => "",
        :profile => "MyText"
      ),
      Member.create!(
        :name => "",
        :image => "",
        :profile => "MyText"
      )
    ])
  end

  it "renders a list of members" do
    render
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
