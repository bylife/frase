require 'rails_helper'

RSpec.describe "members/new", type: :view do
  before(:each) do
    assign(:member, Member.new(
      :name => "",
      :image => "",
      :profile => "MyText"
    ))
  end

  it "renders new member form" do
    render

    assert_select "form[action=?][method=?]", members_path, "post" do

      assert_select "input#member_name[name=?]", "member[name]"

      assert_select "input#member_image[name=?]", "member[image]"

      assert_select "textarea#member_profile[name=?]", "member[profile]"
    end
  end
end
