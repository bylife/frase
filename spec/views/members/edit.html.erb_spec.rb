require 'rails_helper'

RSpec.describe "members/edit", type: :view do
  before(:each) do
    @member = assign(:member, Member.create!(
      :name => "",
      :image => "",
      :profile => "MyText"
    ))
  end

  it "renders the edit member form" do
    render

    assert_select "form[action=?][method=?]", member_path(@member), "post" do

      assert_select "input#member_name[name=?]", "member[name]"

      assert_select "input#member_image[name=?]", "member[image]"

      assert_select "textarea#member_profile[name=?]", "member[profile]"
    end
  end
end
