require 'rails_helper'

RSpec.describe "services/index", type: :view do
  before(:each) do
    assign(:services, [
      Service.create!(
        :title => "Title",
        :url => "Url",
        :image => "Image"
      ),
      Service.create!(
        :title => "Title",
        :url => "Url",
        :image => "Image"
      )
    ])
  end

  it "renders a list of services" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Url".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
  end
end
