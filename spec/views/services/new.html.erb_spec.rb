require 'rails_helper'

RSpec.describe "services/new", type: :view do
  before(:each) do
    assign(:service, Service.new(
      :title => "MyString",
      :url => "MyString",
      :image => "MyString"
    ))
  end

  it "renders new service form" do
    render

    assert_select "form[action=?][method=?]", services_path, "post" do

      assert_select "input#service_title[name=?]", "service[title]"

      assert_select "input#service_url[name=?]", "service[url]"

      assert_select "input#service_image[name=?]", "service[image]"
    end
  end
end
