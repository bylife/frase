require 'rails_helper'

RSpec.describe "talents/show", type: :view do
  before(:each) do
    @talent = assign(:talent, Talent.create!(
      :name => "Name",
      :photo => "Photo",
      :linkedin => "Linkedin"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Photo/)
    expect(rendered).to match(/Linkedin/)
  end
end
