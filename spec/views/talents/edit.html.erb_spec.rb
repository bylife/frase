require 'rails_helper'

RSpec.describe "talents/edit", type: :view do
  before(:each) do
    @talent = assign(:talent, Talent.create!(
      :name => "MyString",
      :photo => "MyString",
      :linkedin => "MyString"
    ))
  end

  it "renders the edit talent form" do
    render

    assert_select "form[action=?][method=?]", talent_path(@talent), "post" do

      assert_select "input#talent_name[name=?]", "talent[name]"

      assert_select "input#talent_photo[name=?]", "talent[photo]"

      assert_select "input#talent_linkedin[name=?]", "talent[linkedin]"
    end
  end
end
