require 'rails_helper'

RSpec.describe "talents/new", type: :view do
  before(:each) do
    assign(:talent, Talent.new(
      :name => "MyString",
      :photo => "MyString",
      :linkedin => "MyString"
    ))
  end

  it "renders new talent form" do
    render

    assert_select "form[action=?][method=?]", talents_path, "post" do

      assert_select "input#talent_name[name=?]", "talent[name]"

      assert_select "input#talent_photo[name=?]", "talent[photo]"

      assert_select "input#talent_linkedin[name=?]", "talent[linkedin]"
    end
  end
end
