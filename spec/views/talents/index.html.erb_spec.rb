require 'rails_helper'

RSpec.describe "talents/index", type: :view do
  before(:each) do
    assign(:talents, [
      Talent.create!(
        :name => "Name",
        :photo => "Photo",
        :linkedin => "Linkedin"
      ),
      Talent.create!(
        :name => "Name",
        :photo => "Photo",
        :linkedin => "Linkedin"
      )
    ])
  end

  it "renders a list of talents" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Photo".to_s, :count => 2
    assert_select "tr>td", :text => "Linkedin".to_s, :count => 2
  end
end
