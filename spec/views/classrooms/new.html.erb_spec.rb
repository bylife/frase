require 'rails_helper'

RSpec.describe "classrooms/new", type: :view do
  before(:each) do
    assign(:classroom, Classroom.new(
      :title => "MyString",
      :course => nil,
      :city => "MyString",
      :value => 1.5,
      :discount => 1.5
    ))
  end

  it "renders new classroom form" do
    render

    assert_select "form[action=?][method=?]", classrooms_path, "post" do

      assert_select "input#classroom_title[name=?]", "classroom[title]"

      assert_select "input#classroom_course_id[name=?]", "classroom[course_id]"

      assert_select "input#classroom_city[name=?]", "classroom[city]"

      assert_select "input#classroom_value[name=?]", "classroom[value]"

      assert_select "input#classroom_discount[name=?]", "classroom[discount]"
    end
  end
end
