require 'rails_helper'

RSpec.describe "classrooms/index", type: :view do
  before(:each) do
    assign(:classrooms, [
      Classroom.create!(
        :title => "Title",
        :course => nil,
        :city => "City",
        :value => 1.5,
        :discount => 1.5
      ),
      Classroom.create!(
        :title => "Title",
        :course => nil,
        :city => "City",
        :value => 1.5,
        :discount => 1.5
      )
    ])
  end

  it "renders a list of classrooms" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
