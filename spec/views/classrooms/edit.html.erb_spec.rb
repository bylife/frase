require 'rails_helper'

RSpec.describe "classrooms/edit", type: :view do
  before(:each) do
    @classroom = assign(:classroom, Classroom.create!(
      :title => "MyString",
      :course => nil,
      :city => "MyString",
      :value => 1.5,
      :discount => 1.5
    ))
  end

  it "renders the edit classroom form" do
    render

    assert_select "form[action=?][method=?]", classroom_path(@classroom), "post" do

      assert_select "input#classroom_title[name=?]", "classroom[title]"

      assert_select "input#classroom_course_id[name=?]", "classroom[course_id]"

      assert_select "input#classroom_city[name=?]", "classroom[city]"

      assert_select "input#classroom_value[name=?]", "classroom[value]"

      assert_select "input#classroom_discount[name=?]", "classroom[discount]"
    end
  end
end
