require 'rails_helper'

RSpec.describe "classrooms/show", type: :view do
  before(:each) do
    @classroom = assign(:classroom, Classroom.create!(
      :title => "Title",
      :course => nil,
      :city => "City",
      :value => 1.5,
      :discount => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(//)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/1.5/)
  end
end
