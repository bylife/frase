require 'rails_helper'

RSpec.describe "boxes/new", type: :view do
  before(:each) do
    assign(:box, Box.new(
      :title => "MyString",
      :body => "MyText",
      :image => "MyString",
      :video => "MyString",
      :page => "MyString"
    ))
  end

  it "renders new box form" do
    render

    assert_select "form[action=?][method=?]", boxes_path, "post" do

      assert_select "input#box_title[name=?]", "box[title]"

      assert_select "textarea#box_body[name=?]", "box[body]"

      assert_select "input#box_image[name=?]", "box[image]"

      assert_select "input#box_video[name=?]", "box[video]"

      assert_select "input#box_page[name=?]", "box[page]"
    end
  end
end
