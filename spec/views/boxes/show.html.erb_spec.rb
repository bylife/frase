require 'rails_helper'

RSpec.describe "boxes/show", type: :view do
  before(:each) do
    @box = assign(:box, Box.create!(
      :title => "Title",
      :body => "MyText",
      :image => "Image",
      :video => "Video",
      :page => "Page"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Image/)
    expect(rendered).to match(/Video/)
    expect(rendered).to match(/Page/)
  end
end
