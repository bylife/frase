require 'rails_helper'

RSpec.describe "boxes/index", type: :view do
  before(:each) do
    assign(:boxes, [
      Box.create!(
        :title => "Title",
        :body => "MyText",
        :image => "Image",
        :video => "Video",
        :page => "Page"
      ),
      Box.create!(
        :title => "Title",
        :body => "MyText",
        :image => "Image",
        :video => "Video",
        :page => "Page"
      )
    ])
  end

  it "renders a list of boxes" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
    assert_select "tr>td", :text => "Video".to_s, :count => 2
    assert_select "tr>td", :text => "Page".to_s, :count => 2
  end
end
