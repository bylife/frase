require 'rails_helper'

RSpec.describe "videos/new", type: :view do
  before(:each) do
    assign(:video, Video.new(
      :title => "MyString",
      :video_file => "MyString",
      :page => "MyString"
    ))
  end

  it "renders new video form" do
    render

    assert_select "form[action=?][method=?]", videos_path, "post" do

      assert_select "input#video_title[name=?]", "video[title]"

      assert_select "input#video_video_file[name=?]", "video[video_file]"

      assert_select "input#video_page[name=?]", "video[page]"
    end
  end
end
