require 'rails_helper'

RSpec.describe "videos/index", type: :view do
  before(:each) do
    assign(:videos, [
      Video.create!(
        :title => "Title",
        :video_file => "Video File",
        :page => "Page"
      ),
      Video.create!(
        :title => "Title",
        :video_file => "Video File",
        :page => "Page"
      )
    ])
  end

  it "renders a list of videos" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Video File".to_s, :count => 2
    assert_select "tr>td", :text => "Page".to_s, :count => 2
  end
end
