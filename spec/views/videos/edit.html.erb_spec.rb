require 'rails_helper'

RSpec.describe "videos/edit", type: :view do
  before(:each) do
    @video = assign(:video, Video.create!(
      :title => "MyString",
      :video_file => "MyString",
      :page => "MyString"
    ))
  end

  it "renders the edit video form" do
    render

    assert_select "form[action=?][method=?]", video_path(@video), "post" do

      assert_select "input#video_title[name=?]", "video[title]"

      assert_select "input#video_video_file[name=?]", "video[video_file]"

      assert_select "input#video_page[name=?]", "video[page]"
    end
  end
end
