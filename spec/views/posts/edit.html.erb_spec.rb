require 'rails_helper'

RSpec.describe "posts/edit", type: :view do
  before(:each) do
    @post = assign(:post, Post.create!(
      :title => "MyString",
      :thumbnail => "MyString",
      :body => "MyText",
      :slug => "MyString",
      :user_id => 1,
      :status => "MyString",
      :date => "MyString"
    ))
  end

  it "renders the edit post form" do
    render

    assert_select "form[action=?][method=?]", post_path(@post), "post" do

      assert_select "input#post_title[name=?]", "post[title]"

      assert_select "input#post_thumbnail[name=?]", "post[thumbnail]"

      assert_select "textarea#post_body[name=?]", "post[body]"

      assert_select "input#post_slug[name=?]", "post[slug]"

      assert_select "input#post_user_id[name=?]", "post[user_id]"

      assert_select "input#post_status[name=?]", "post[status]"

      assert_select "input#post_date[name=?]", "post[date]"
    end
  end
end
