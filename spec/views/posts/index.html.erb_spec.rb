require 'rails_helper'

RSpec.describe "posts/index", type: :view do
  before(:each) do
    assign(:posts, [
      Post.create!(
        :title => "Title",
        :thumbnail => "Thumbnail",
        :body => "MyText",
        :slug => "Slug",
        :user_id => 1,
        :status => "Status",
        :date => "Date"
      ),
      Post.create!(
        :title => "Title",
        :thumbnail => "Thumbnail",
        :body => "MyText",
        :slug => "Slug",
        :user_id => 1,
        :status => "Status",
        :date => "Date"
      )
    ])
  end

  it "renders a list of posts" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Thumbnail".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Slug".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Date".to_s, :count => 2
  end
end
