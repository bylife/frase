require 'rails_helper'

RSpec.describe "posts/new", type: :view do
  before(:each) do
    assign(:post, Post.new(
      :title => "MyString",
      :thumbnail => "MyString",
      :body => "MyText",
      :slug => "MyString",
      :user_id => 1,
      :status => "MyString",
      :date => "MyString"
    ))
  end

  it "renders new post form" do
    render

    assert_select "form[action=?][method=?]", posts_path, "post" do

      assert_select "input#post_title[name=?]", "post[title]"

      assert_select "input#post_thumbnail[name=?]", "post[thumbnail]"

      assert_select "textarea#post_body[name=?]", "post[body]"

      assert_select "input#post_slug[name=?]", "post[slug]"

      assert_select "input#post_user_id[name=?]", "post[user_id]"

      assert_select "input#post_status[name=?]", "post[status]"

      assert_select "input#post_date[name=?]", "post[date]"
    end
  end
end
