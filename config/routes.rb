Rails.application.routes.draw do
    

    mount RedactorRails::Engine => '/redactor_rails'
    resources :user_sessions, only: [:create, :new, :destroy]
  
    get 'admin', to: 'dashboard#index', as: '/'
    get 'customers/services' => 'customers#services'
    get 'customers/courses' => 'customers#courses'
    get 'types/:slug/jobs' => 'types#jobs'
    get 'courses/:slug/courses' => 'coursess#courses'
    get 'modalities/courses' => 'modalities#courses'
    get 'headers/page/:page' => 'headers#page'
    get 'contacts/addnewsletter' => 'contacts#addnewsletter'
    get 'talents/categories' => 'talents#categories'
    get 'tags/:tag', to: 'posts#tags', as: :tag

    get 'members/team' => 'members#team'
    get 'courses/bymodality' => 'courses#bymodality'
    get 'courses/pagamentoretorno' => 'courses#pagamentoretorno'
    get 'classrooms/nextcourses' => 'classrooms#nextcourses'
    get 'classrooms/opened' => 'classrooms#opened'
    
    resources :subscriptions
    resources :students
    resources :headers
    resources :modalities
    resources :courses
    resources :categories
    resources :members
    resources :types
    resources :users
    resources :videos
    resources :customers
    resources :jobs
    resources :talents
    resources :services
    resources :posts
    resources :testimonies
    resources :vouchers
    resources :classrooms
    resources :boxes
    resources :user_sessions
  # namespace :admin do
  # end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".


  root to: "home#index"  

  scope '/api' do
    get 'headers/page/:page' => 'headers#page'
    get 'customers/services' => 'customers#services'
    get 'customers/courses' => 'customers#courses'
    get 'modalities/courses' => 'modalities#courses'
    get 'talents/categories' => 'talents#categories'
    get 'posts/publisheds' => 'posts#publisheds'
    get 'classrooms/nextcourses' => 'classrooms#nextcourses'
    get 'classrooms/opened' => 'classrooms#opened'
    resources :subscriptions, defaults: {format: :json}
    resources :students, defaults: {format: :json}
    resources :modalities, defaults: {format: :json}
    resources :courses, defaults: {format: :json}
    resources :classrooms, defaults: {format: :json}
    resources :headers, defaults: {format: :json}
    resources :testimonies, defaults: {format: :json}
    resources :posts, defaults: {format: :json}
    resources :categories, defaults: {format: :json}
    resources :services, defaults: {format: :json}
    resources :customers, defaults: {format: :json}
    resources :members, defaults: {format: :json}
    resources :talents, defaults: {format: :json}
    resources :jobs, defaults: {format: :json}
    resources :videos, defaults: {format: :json}
    resources :types, defaults: {format: :json}    
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
