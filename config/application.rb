require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'carrierwave'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Frase
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.time_zone = 'Brasilia'
    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.angular_templates.module_name    = 'templates'
    config.angular_templates.ignore_prefix  = %w(templates/)
    config.angular_templates.inside_paths   = [Rails.root.join('app', 'assets')]
    config.angular_templates.markups        = %w(erb str html)
    config.angular_templates.htmlcompressor = false

    #config.action_dispatch.default_headers = {
    #    'Access-Control-Allow-Origin' => '*',
    #    'Access-Control-Request-Method' => '*',
    #    'X-Frame-Options' => 'ALLOWALL',
    #    'X-XSS-Protection' => '1; mode=block',
    #}
    config.i18n.default_locale = :"pt-BR"
    #config.assets.paths << Rails.root.join('vendor', 'assets', 'components', 'bootstrap-sass-official', 'assets', 'fonts' 'templates')
    # Bower asset paths
    root.join('vendor', 'assets', 'components', 'bootstrap-sass-official', 'assets', 'fonts' 'templates').to_s.tap do |bower_path|
      config.sass.load_paths << bower_path
      config.assets.paths << bower_path
    end
    # Precompile Bootstrap fonts
    config.assets.precompile << %r(bootstrap-sass-official/assets/fonts/bootstrap/[\w-]+\.(?:eot|svg|ttf|woff2?)$)
    # Minimum Sass number precision required by bootstrap-sass
    #::Sass::Script::Value::Number.precision = [8, ::Sass::Script::Value::Number.precision].max
    #config.assets.precompile += %w( .svg .eot .woff .ttf)

    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        #origins 'testes.bylife.com.br', 'localhost:9000'
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :options]
      end
    end
  end
end
