(function() {
  'use strict';

  /**
    * @ngdoc directive
    * @name fraseApp.directive:contactService
    * @description
    * # contactService
   */
  angular.module('fraseApp').directive('contactService', function() {
    return {
      restrict: 'EA',
      templateUrl: 'views/contact-service.html'
    };
  });

}).call(this);

//# sourceMappingURL=contactservice.js.map
