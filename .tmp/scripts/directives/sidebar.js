(function() {
  'use strict';

  /**
    * @ngdoc directive
    * @name fraseApp.directive:sidebar
    * @description
    * # Sidebar
   */
  angular.module('fraseApp').directive('sidebarBlog', function() {
    return {
      restrict: 'EA',
      templateUrl: 'views/sidebar.html',
      controller: function($scope) {
        $scope.next_courses = [
          {
            city: 'SÃO PAULO',
            date: '08 E 09/FEV',
            title: 'Roteiro para Vídeos Estratégicos',
            url: 'http://www.google.com.br'
          }, {
            city: 'FLORIANÓPOLIS',
            date: '05/MAR',
            title: 'Conteúdo Estratégico para Mídias Sociais',
            url: 'http://www.google.com.br'
          }
        ];
        $scope.recent_posts = [
          {
            title: 'Teste',
            url: 'http://www.bylife.com.br'
          }, {
            title: 'Teste2',
            url: 'http://www.byphisio.com.br'
          }
        ];
        $scope.archive = [
          {
            title: 'Teste',
            url: 'http://www.bylife.com.br'
          }, {
            title: 'Teste2',
            url: 'http://www.byphisio.com.br'
          }
        ];
        return $scope.categories = [
          {
            title: 'Agenda',
            url: '#/blog/categorias/agenda'
          }, {
            title: 'Dicas',
            url: '#/blog/categorias/dicas'
          }, {
            title: 'Treinamentos',
            url: '#/blog/categorias/treinamentos'
          }, {
            title: 'Serviços',
            url: '#/blog/categorias/servicos'
          }
        ];
      }
    };
  });

}).call(this);

//# sourceMappingURL=sidebar.js.map
