(function() {
  'use strict';

  /**
    * @ngdoc directive
    * @name fraseApp.directive:missionBox
    * @description
    * # missionBox
   */
  angular.module('fraseApp').directive('missionBox', function() {
    return {
      restrict: 'EA',
      templateUrl: '_mission-box.html',
      replace: true
    };
  });

}).call(this);

//# sourceMappingURL=missionbox.js.map
