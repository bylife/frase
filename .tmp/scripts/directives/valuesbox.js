(function() {
  'use strict';

  /**
    * @ngdoc directive
    * @name fraseApp.directive:valuesBox
    * @description
    * # valuesBox
   */
  angular.module('fraseApp').directive('valuesBox', function() {
    return {
      restrict: 'EA',
      templateUrl: 'views/values-box.html',
      replace: true
    };
  });

}).call(this);

//# sourceMappingURL=valuesbox.js.map
