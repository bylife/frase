(function() {
  'use strict';

  /**
    * @ngdoc directive
    * @name fraseApp.directive:formInscricao
    * @description
    * # formInscricao
   */
  angular.module('fraseApp').directive('formInscricao', function() {
    return {
      restrict: 'EA',
      templateUrl: 'views/form-inscricao.html',
      controller: function($scope) {
        $scope.course = {
          id: 1,
          city: 'SÃO PAULO',
          date: '08 E 09/FEV',
          title: 'Roteiro para Vídeos Estratégicos',
          full_value: 810.00,
          discount_value: 769.00
        };
        $scope.step = 1;
        $scope.nextStep = function() {
          return $scope.step++;
        };
        $scope.prevStep = function() {
          return $scope.step--;
        };
        return $scope.selectStep = function(id) {
          return $scope.step = id;
        };
      }
    };
  });

}).call(this);

//# sourceMappingURL=form-inscricao.js.map
