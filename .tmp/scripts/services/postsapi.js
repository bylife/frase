(function() {
  'use strict';

  /**
    * @ngdoc service
    * @name fraseApp.PostsApi
    * @description
    * # PostsApi
    * Service in the fraseApp.
   */
  angular.module('fraseApp').service('PostsApi', [
    '$http', '$rootScope', 'ajax', function($http, $rootScope, ajax) {
      return {
        get_posts: function() {
          console.log('getting posts...');
          return ajax.get($rootScope.appUrl + 'mock/posts.json');
        },
        get_post: function(params) {
          console.log('getting post...');
          return ajax.get($rootScope.appUrl + 'mock/posts.json', params);
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=postsapi.js.map
