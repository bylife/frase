(function() {
  'use strict';

  /**
    * @ngdoc service
    * @name fraseApp.modajax
    * @description
    * # modajax
    * Factory in the fraseApp.
   */
  angular.module('modajax', []).factory('ajax', [
    '$http', function($http) {
      return {
        get: function(url, params) {
          if (!params) {
            params = {};
          }
          return $http({
            method: 'GET',
            url: url,
            params: params
          });
        },
        post: function(url, params) {
          if (!params) {
            params = {};
          }
          return $http({
            method: 'POST',
            url: url,
            data: params
          });
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=modajax.js.map
