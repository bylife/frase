(function() {
  'use strict';

  /**
    * @ngdoc service
    * @name fraseApp.ajax
    * @description
    * # ajax
    * Factory in the fraseApp.
   */
  angular.module('modajax', []).factory('ajax', [
    '$http', function($http) {
      return {
        get: function(url, params) {
          if (!params) {
            params = {};
          }
          return $http({
            method: 'GET',
            url: url,
            params: params
          });
        },
        post: function(url, params) {
          if (!params) {
            params = {};
          }
          return $http({
            method: 'POST',
            url: url,
            data: params
          });
        },
        put: function(url, params) {
          if (!params) {
            params = {};
          }
          return $http({
            method: 'PUT',
            url: url,
            data: params
          });
        },
        del: function(url, params) {
          if (!params) {
            params = {};
          }
          return $http({
            method: 'DELETE',
            url: url,
            data: params
          });
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=ajax.js.map
