(function() {
  'use strict';

  /**
    * @ngdoc service
    * @name fraseApp.CoursesApi
    * @description
    * # CoursesApi
    * Service in the fraseApp.
   */
  angular.module('fraseApp').service('CoursesApi', [
    '$http', '$rootScope', 'ajax', function($http, $rootScope, ajax) {
      return {
        get_courses: function() {
          console.log('getting courses...');
          return ajax.get($rootScope.appUrl + 'mock/courses.json');
        },
        get_course: function(params) {
          console.log('getting course...');
          return ajax.get($rootScope.appUrl + 'mock/courses.json', params);
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=coursesapi.js.map
