(function() {
  'use strict';
  var onGoogleReady;

  onGoogleReady = function() {
    return angular.bootstrap(document.getElementById("map"), ['fraseApp.ui-map']);
  };


  /**
    * @ngdoc overview
    * @name fraseApp
    * @description
    * # fraseApp
    *
    * Main module of the application.
   */

  angular.module('fraseApp', ['ngAnimate', 'ngCookies', 'ngResource', 'ngRoute', 'ngSanitize', 'ngTouch', 'ui.bootstrap', 'ui.event', 'modajax', 'ui.map']).config(function($routeProvider) {
    return $routeProvider.when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    }).when('/a-empresa', {
      templateUrl: 'views/aempresa.html',
      controller: 'AboutCtrl'
    }).when('/servicos', {
      templateUrl: 'views/services.html',
      controller: 'ServicesCtrl'
    }).when('/cursos', {
      templateUrl: 'views/courses.html',
      controller: 'CoursesCtrl'
    }).when('/cursos/:slug', {
      templateUrl: 'views/courses-single.html',
      controller: 'CoursesViewCtrl'
    }).when('/servicos/:slug', {
      templateUrl: 'views/servicos-single.html',
      controller: 'ServicesSingleCtrl'
    }).when('/portfolio', {
      templateUrl: 'views/portfolio.html',
      controller: 'JobsCtrl'
    }).when('/portfolio/:slug', {
      templateUrl: 'views/portfolio-single.html',
      controller: 'JobsSingleCtrl'
    }).when('/banco-de-talentos', {
      templateUrl: 'views/banco-de-talentos.html',
      controller: 'TalentsCtrl'
    }).when('/depoimentos', {
      templateUrl: 'views/depoimentos.html',
      controller: 'TestimoniesCtrl'
    }).when('/quem-atendemos', {
      templateUrl: 'views/quem-atendemos.html',
      controller: 'CustomersCtrl'
    }).when('/contato', {
      templateUrl: 'views/contato.html',
      controller: 'ContactCtrl'
    }).when('/blog', {
      templateUrl: 'views/blog.html',
      controller: 'BlogCtrl'
    }).when('/curso-in-company', {
      templateUrl: 'views/curso-in-company.html',
      controller: 'CoursesCtrl'
    }).when('/curso-online', {
      templateUrl: 'views/curso-online.html',
      controller: 'CoursesViewCtrl'
    }).when('/curso-presencial', {
      templateUrl: 'views/curso-presencial.html',
      controller: 'CoursesViewCtrl'
    }).when('/cadastro-cursos', {
      templateUrl: 'views/cadastro-cursos.html',
      controller: 'CoursesCtrl'
    }).otherwise({
      redirectTo: '/'
    });
  });

  angular.module('fraseApp').controller('AppCtrl', [
    '$scope', '$rootScope', function($scope, $rootScope) {
      return $rootScope.appUrl = 'http://localhost:9000/';
    }
  ]);

}).call(this);

//# sourceMappingURL=app.js.map
