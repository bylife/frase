(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:CustomersCtrl
    * @description
    * # CustomersCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('CustomersCtrl', function($scope) {
    return $scope.customers = [
      {
        url: '../images/yeoman.png',
        title: 'Yeoman'
      }
    ];
  });

}).call(this);

//# sourceMappingURL=customers.js.map
