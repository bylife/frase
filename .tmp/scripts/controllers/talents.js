(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:TalentsCtrl
    * @description
    * # TalentsCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('TalentsCtrl', function($scope) {
    return $scope.categories = [
      {
        category: 'VER TODOS',
        talents: [
          {
            name: 'Ricardo Ribeiro',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }, {
            name: 'Davi Martimiano',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }, {
            name: 'Barbara Rocha',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }, {
            name: 'Rodolfo Dantas',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }, {
            name: 'Alberto',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }, {
            name: 'Alice',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }
        ]
      }, {
        category: 'A',
        talents: [
          {
            name: 'Alberto',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }, {
            name: 'Alice',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }
        ]
      }, {
        category: 'B',
        talents: [
          {
            name: 'Barbara Rocha',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }
        ]
      }, {
        category: 'D',
        talents: [
          {
            name: 'Davi Martimiano',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }
        ]
      }, {
        category: 'R',
        talents: [
          {
            name: 'Rodolfo Dantas',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }, {
            name: 'Ricardo Ribeiro',
            image: 'images/unknown_talent.png',
            link: 'http://www.bylife.com.br'
          }
        ]
      }
    ];
  });

}).call(this);

//# sourceMappingURL=talents.js.map
