(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:CoursesviewCtrl
    * @description
    * # CoursesviewCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('CoursesViewCtrl', [
    '$scope', 'CoursesApi', function($scope, CoursesApi) {
      $scope.faqs = [
        {
          question: 'Por que este curso é online?',
          answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis explicabo, nihil id iste officia cumque porro.'
        }, {
          question: 'A partir de quando posso me inscrever?',
          answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis explicabo, nihil id iste officia cumque porro.'
        }, {
          question: 'Como terei acesso ao curso online?',
          answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis explicabo, nihil id iste officia cumque porro.'
        }, {
          question: 'Quando poderei iniciar o curso?',
          answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis explicabo, nihil id iste officia cumque porro.'
        }, {
          question: 'Quais são os recursos didáticos empregados nesse curso?',
          answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis explicabo, nihil id iste officia cumque porro.'
        }, {
          question: 'Como fazer a inscrição?',
          answer: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis explicabo, nihil id iste officia cumque porro.'
        }
      ];
      $scope.oneAtATime = true;
      $scope.next_courses = [
        {
          city: 'SÃO PAULO',
          date: '08 E 09/FEV',
          title: 'Roteiro para Vídeos Estratégicos',
          url: 'http://www.google.com.br'
        }, {
          city: 'FLORIANÓPOLIS',
          date: '05/MAR',
          title: 'Conteúdo Estratégico para Mídias Sociais',
          url: 'http://www.google.com.br'
        }
      ];
      $scope.open_courses = [
        {
          city: 'SÃO PAULO',
          date: '08 E 09/FEV',
          time: '9h às 18h',
          place: 'Espaço Paulista de Eventos Av. Paulista, 807, 17º andar São Paulo-SP'
        }, {
          city: 'FLORIANÓPOLIS',
          date: '05/FEV',
          time: '9h às 18h',
          place: 'Hotel Boulevard Av. Higienópolis, 199, Florianópolis-SC'
        }, {
          city: 'LONDRINA',
          date: '08 E 09/FEV',
          time: '9h às 18h',
          place: 'Espaço Paulista de Eventos Av. Paulista, 807, 17º andar Londrina-PR'
        }
      ];
      $scope.partners = [
        {
          title: 'ABRINQ',
          url: 'images/abrinq.jpg'
        }, {
          title: 'Akzo Nobel',
          url: 'images/akzo-nobel.jpg'
        }, {
          title: 'Bebelu',
          url: 'images/bebelu.jpg'
        }, {
          title: 'Embraer',
          url: 'images/embraer.jpg'
        }
      ];
      return $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=coursesview.js.map
