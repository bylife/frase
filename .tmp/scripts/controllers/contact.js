(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:ContactCtrl
    * @description
    * # ContactCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('ContactCtrl', function($scope) {
    var lat, lng;
    $scope.mapOptions = {
      center: new google.maps.LatLng(-23.1969716, -45.9091396),
      scrollwheel: false,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    $scope.myMarkers = [];
    lat = -23.1969716;
    lng = -45.9091396;
    $scope.$watch('myMap', function(map) {
      var marker;
      if (map) {
        return marker = new google.maps.Marker({
          map: $scope.myMap,
          position: new google.maps.LatLng(-23.1969716, -45.9091396),
          title: 'Frase Conteúdo estratégico'
        });
      }
    });
    $scope.openMarkerInfo = function(marker) {
      $scope.currentMarker = marker;
      $scope.currentMarkerLat = marker.getPosition().lat();
      $scope.currentMarkerLng = marker.getPosition().lng();
      return $scope.myInfoWindow.open($scope.myMap, marker);
    };
    return $scope.setMarkerPosition = function(marker, lat, lng) {
      return marker.setPosition(new google.maps.LatLng(lat, lng));
    };
  });

}).call(this);

//# sourceMappingURL=contact.js.map
