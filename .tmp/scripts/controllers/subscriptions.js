(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:SubscriptionsCtrl
    * @description
    * # SubscriptionsCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('SubscriptionsCtrl', [
    '$scope', function($scope) {
      return $scope.teste = '';
    }
  ]);

}).call(this);

//# sourceMappingURL=subscriptions.js.map
