(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:MainCtrl
    * @description
    * # MainCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('MainCtrl', [
    '$scope', '$rootScope', function($scope, $rootScope) {
      var first, many, whatDevice;
      $scope.services = [
        {
          url: '#/servicos/roteiro-para-video-institucional',
          title: 'Roteiro para Vídeo Institucional',
          image: 'images/service01.jpg'
        }, {
          url: '#/servicos/gestao-de-midias-sociais',
          title: 'Gestão de Mídias Sociais',
          image: 'images/service01.jpg'
        }, {
          url: '#/servicos/planejamento-de-comunicacao',
          title: 'Planejamento de Comunicação',
          image: 'images/service01.jpg'
        }, {
          url: '#/servicos/webwriting',
          title: 'Webwriting',
          image: 'images/service01.jpg'
        }, {
          url: '#/servicos/comunicacao-interna',
          title: 'Comunicação Interna',
          image: 'images/service01.jpg'
        }, {
          url: '#/servicos/comunicacao-publicitaria',
          title: 'Criação publicitária',
          image: 'images/service01.jpg'
        }, {
          url: '#/servicos/consultoria-criativa',
          title: 'Consultoria criativa',
          image: 'images/service01.jpg'
        }, {
          url: '#/servicos/producao-de-webvideos-e-canais',
          title: 'Produção de webvideos e canais',
          image: 'images/service01.jpg'
        }
      ];
      $scope.mission = {
        text: 'A missão do conteúdo é garantir que os resultados almejados por uma organização sejam atingidos por meio de uma comunicação eficaz.'
      };
      $scope.testimony = {
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque veritatis veniam rem ducimus odit ipsam voluptatibus fugit sunt, vitae voluptates dolorum expedita, culpa distinctio porro quam, ullam inventore maiores! Sequi.'
      };
      $scope.next_courses = [
        {
          city: 'SÃO PAULO',
          date: '08 E 09/FEV',
          title: 'Roteiro para Vídeos Estratégicos'
        }, {
          city: 'FLORIANÓPOLIS',
          date: '05/MAR',
          title: 'Conteúdo Estratégico para Mídias Sociais'
        }
      ];
      $scope.myInterval = 5000;
      $scope.courses = [
        {
          type: 'VER TODOS',
          slides: [
            {
              active: true,
              url: '',
              image: 'images/thumb_webwriting.jpg',
              title: 'Webwriting2'
            }, {
              active: true,
              url: '',
              image: 'images/thumb_webwriting.jpg',
              title: 'Webwriting'
            }, {
              active: true,
              url: '',
              image: 'images/thumb_redpub.jpg',
              title: 'Redação Publicitária'
            }, {
              active: true,
              url: '',
              image: 'images/thumb_rotvid.jpg',
              title: 'Roteiro para Vídeos'
            }
          ]
        }, {
          type: 'ONLINE',
          slides: [
            {
              active: true,
              url: '',
              image: 'images/thumb_rotvid.jpg',
              title: 'Roteiro para Vídeos'
            }, {
              active: true,
              url: '',
              image: 'images/thumb_webwriting.jpg',
              title: 'Webwriting'
            }, {
              active: true,
              url: '',
              image: 'images/thumb_redpub.jpg',
              title: 'Redação Publicitária'
            }
          ]
        }, {
          type: 'PRESENCIAL',
          slides: [
            {
              active: true,
              url: '',
              image: 'images/thumb_redpub.jpg',
              title: 'Redação Publicitária'
            }, {
              active: true,
              url: '',
              image: 'images/thumb_webwriting.jpg',
              title: 'Webwriting'
            }, {
              active: true,
              url: '',
              image: 'images/thumb_rotvid.jpg',
              title: 'Roteiro para Vídeos'
            }
          ]
        }
      ];
      $scope.displayMode = 'mobile';
      whatDevice = $scope.nowDevice;
      $scope.$watch('displayMode', function(value) {
        switch (value) {
          case 'mobile':
            return console.log(value);
          case 'tablet':
            return console.log(value);
        }
      });
      first = [];
      many = 1;
      $scope.displayMode = "tablet";
      if ($scope.displayMode === "mobile") {
        many = 1;
      } else if ($scope.displayMode === "tablet") {
        many = 2;
      } else {
        many = 3;
      }
      return angular.forEach($scope.courses, function(course, key) {
        var i, second, slides, _i, _len, _ref;
        _ref = course.slides;
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
          slides = _ref[i];
          console.log('Slides: ' + i);
          second = {
            image1: $scope.courses[key].slides[i]
          };
          if ($scope.courses[key].slides[i + 1] && (many === 2 || many === 3)) {
            second.image2 = $scope.courses[key].slides[i + 1];
          }
          if ($scope.courses[key].slides[i + (many - 1)] && many === 3) {
            second.image3 = $scope.courses[key].slides[i + 2];
          }
          first.push(second);
        }
        return $scope.courses[key].slides = first;
      });
    }
  ]);

  angular.module('fraseApp').directive('dnDisplayMode', function($window) {
    return {
      restrict: 'A',
      scope: {
        dnDisplayMode: '='
      },
      template: '<span class="mobile"></span><span class="tablet"></span><span class="tablet-landscape"></span><span class="desktop"></span>',
      link: function(scope, elem, attrs) {
        var isVisible, markers, update;
        markers = elem.find('span');
        isVisible = function(element) {
          return element && element.style.display !== 'none' && element.offsetWidth && element.offsetHeight;
        };
        update = function() {
          return angular.forEach(markers, function(element) {
            if (isVisible(element)) {
              scope.dnDisplayMode = element.className;
              return false;
            }
          });
        };
        angular.element($window).bind('resize', function() {
          var t;
          clearTimeout(t);
          return t = setTimeout(function() {
            update();
            return scope.$apply();
          }, 300);
        });
        return update();
      }
    };
  });

  angular.module('formApp', ['ngAnimate', 'ui.router']).config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.state('form', {
      url: '/cadastro-cursos',
      templateUrl: 'form.html',
      controller: 'formController'
    }).state('form.profile', {
      url: '/informacoes-pessoais',
      templateUrl: 'form-profile.html'
    }).state('form.interests', {
      url: '/localizacao',
      templateUrl: 'form-interests.html'
    }).state('form.payment', {
      url: '/pagamento',
      templateUrl: 'form-payment.html'
    });
    $urlRouterProvider.otherwise('/form/profile');
  }).controller('formController', function($scope) {
    $scope.formData = {};
    $scope.processForm = function() {
      alert('awesome!');
    };
  });

}).call(this);

//# sourceMappingURL=main.js.map
