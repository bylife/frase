(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:ServicessingleCtrl
    * @description
    * # ServicessingleCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('ServicesSingleCtrl', function($scope, $routeParams) {
    var load;
    $scope.services = [
      {
        url: '#/servicos/roteiro-para-video-institucional',
        title: 'Roteiro para Vídeo Institucional',
        image: 'images/service01.jpg'
      }, {
        url: '#/servicos/gestao-de-midias-sociais',
        title: 'Gestão de Mídias Sociais',
        image: 'images/service01.jpg'
      }, {
        url: '#/servicos/planejamento-de-comunicacao',
        title: 'Planejamento de Comunicação',
        image: 'images/service01.jpg'
      }, {
        url: '#/servicos/webwriting',
        title: 'Webwriting',
        image: 'images/service01.jpg'
      }, {
        url: '#/servicos/comunicacao-interna',
        title: 'Comunicação Interna',
        image: 'images/service01.jpg'
      }, {
        url: '#/servicos/comunicacao-publicitaria',
        title: 'Criação publicitária',
        image: 'images/service01.jpg'
      }, {
        url: '#/servicos/consultoria-criativa',
        title: 'Consultoria criativa',
        image: 'images/service01.jpg'
      }, {
        url: '#/servicos/producao-de-webvideos-e-canais',
        title: 'Produção de webvideos e canais',
        image: 'images/service01.jpg'
      }
    ];
    return load = function() {
      var slug;
      return slug = $routeParams('slug');
    };
  });

}).call(this);

//# sourceMappingURL=servicessingle.js.map
