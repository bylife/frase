(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:BlogCtrl
    * @description
    * # BlogCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('BlogCtrl', [
    '$scope', 'PostsApi', function($scope, PostsApi) {
      $scope.posts = [];
      $scope.getPosts = function() {
        var req;
        req = PostsApi.get_posts();
        return req.then(function(response) {
          if (response.data) {
            return $scope.posts = response.data;
          } else {
            return console.log('Falha ao carregar posts');
          }
        });
      };
      $scope.getPosts();
      $scope.totalItems = $scope.posts.length;
      $scope.currentPage = 1;
      return $scope.setPage = function(pageNo) {
        return $scope.currentPage = pageNo;
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=blog.js.map
