(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:TestimoniesCtrl
    * @description
    * # TestimoniesCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('TestimoniesCtrl', function($scope) {
    $scope.testimonies = [
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque veritatis veniam rem ducimus odit ipsam voluptatibus fugit sunt, vitae voluptates dolorum expedita, culpa distinctio porro quam, ullam inventore maiores! Sequi.'
      }, {
        text: 'Adipisicing elit. Consequuntur provident alias consectetur saepe esse odio quia aut unde, deserunt minus voluptate maxime quasi doloremque. Architecto sunt, dolore tempore eos beatae.'
      }, {
        text: 'Neque veritatis veniam rem ducimus odit ipsam voluptatibus fugit sunt, vitae voluptates dolorum expedita, culpa distinctio porro quam, ullam inventore maiores! Sequi.'
      }
    ];
    $scope.totalItems = $scope.testimonies.length;
    $scope.currentPage = 1;
    return $scope.setPage = function(pageNo) {
      return $scope.currentPage = pageNo;
    };
  });

}).call(this);

//# sourceMappingURL=testimonies.js.map
