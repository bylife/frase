(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:JobsCtrl
    * @description
    * # JobsCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('JobsCtrl', function($scope) {
    return $scope.categories = [
      {
        category: 'VER TODOS',
        jobs: [
          {
            id: 1,
            title: 'Roteiro pra cliente XXXXX',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 2,
            title: 'Produção de conteúdo para ACTUS',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 3,
            title: 'Texto para folder ERIONE',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 4,
            title: 'Texto para folder Clínica Médico Fácil',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }
        ]
      }, {
        category: 'IMAGEM',
        jobs: [
          {
            id: 4,
            title: 'Texto para folder ERIONE',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 5,
            title: 'Texto para folder Clínica Médico Fácil',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }
        ]
      }, {
        category: 'SOM',
        jobs: [
          {
            id: 4,
            title: 'Texto para folder ERIONE',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 5,
            title: 'Texto para folder Clínica Médico Fácil',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }
        ]
      }, {
        category: 'TEXTO',
        jobs: [
          {
            id: 4,
            title: 'Texto para folder ERIONE',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 5,
            title: 'Texto para folder Clínica Médico Fácil',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }
        ]
      }, {
        category: 'VIDEO',
        jobs: [
          {
            id: 4,
            title: 'Texto para folder ERIONE',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 5,
            title: 'Texto para folder Clínica Médico Fácil',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }
        ]
      }, {
        category: 'MÍDIAS DIGITAIS',
        jobs: [
          {
            id: 4,
            title: 'Texto para folder ERIONE',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }, {
            id: 5,
            title: 'Texto para folder Clínica Médico Fácil',
            img: 'images/job01.jpg',
            url: '#/portfolio/producao-de-conteudo-para-actus'
          }
        ]
      }
    ];
  });

}).call(this);

//# sourceMappingURL=jobs.js.map
