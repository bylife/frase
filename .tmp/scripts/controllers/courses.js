(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:CoursesCtrl
    * @description
    * # CoursesCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('CoursesCtrl', [
    '$scope', 'CoursesApi', function($scope, CoursesApi) {
      $scope.categories = [];
      $scope.getCourses = function() {
        var req;
        req = CoursesApi.get_courses();
        return req.then(function(response) {
          if (response.data) {
            return $scope.categories = response.data;
          } else {
            return console.log('Falha ao carregar cursos');
          }
        });
      };
      return $scope.getCourses();
    }
  ]);

}).call(this);

//# sourceMappingURL=courses.js.map
