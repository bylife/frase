(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:AboutCtrl
    * @description
    * # AboutCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('AboutCtrl', [
    '$scope', '$rootScope', function($scope, $rootScope) {
      $scope.team = [
        {
          id: 1,
          name: 'Rodolfo Dantas',
          avatar: 'images/rodolfo.jpg',
          profile: '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet repellendus inventore maxime optio iste a assumenda commodi quae nostrum ex doloremque deserunt minus provident saepe dicta ea fugiat error, fugit.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet repellendus inventore maxime optio iste a assumenda commodi quae nostrum ex doloremque deserunt minus provident saepe dicta ea fugiat error, fugit.</p>'
        }, {
          id: 2,
          name: 'Monique Baraúna',
          avatar: 'images/monique.jpg',
          profile: 'Monique ipsum dolor sit amet, consectetur adipisicing elit. Amet repellendus inventore maxime optio iste a assumenda commodi quae nostrum ex doloremque deserunt minus provident saepe dicta ea fugiat error, fugit.'
        }, {
          id: 3,
          name: 'Cristiane Fayer',
          avatar: 'images/cristiane.jpg',
          profile: 'Cris ipsum dolor sit amet, consectetur adipisicing elit. Amet repellendus inventore maxime optio iste a assumenda commodi quae nostrum ex doloremque deserunt minus provident saepe dicta ea fugiat error, fugit.'
        }, {
          id: 4,
          name: 'Bruna Cleto',
          avatar: 'images/bruna.jpg',
          profile: 'Bruna ipsum dolor sit amet, consectetur adipisicing elit. Amet repellendus inventore maxime optio iste a assumenda commodi quae nostrum ex doloremque deserunt minus provident saepe dicta ea fugiat error, fugit.'
        }
      ];
      $scope.mission = {
        text: 'Prestar serviços com qualidade, diferenciação, agilidade e um atendimento primoroso, a empresas e pessoas que buscam resultados mercadológicos efeticos, contribundo para o seu crescimento'
      };
      $scope.values = {
        text: 'Assertividade, comprometimento, humildade, relacionamento de qualidade com todas as pessoas, diferenciação sempre, aprimoramento constante, responsabilidade social e ambiental, organização e planejamento'
      };
      $scope.showProfile = function(id) {
        return $scope.activeProfile = id;
      };
      return $scope.pointerPosition = function(id) {
        var pos;
        switch (id) {
          case 1:
            pos = 'first';
            break;
          case 2:
            pos = 'second';
            break;
          case 3:
            pos = 'third';
            break;
          case 4:
            pos = 'forth';
        }
        return pos;
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=about.js.map
