(function() {
  'use strict';

  /**
    * @ngdoc function
    * @name fraseApp.controller:JobssingleCtrl
    * @description
    * # JobssingleCtrl
    * Controller of the fraseApp
   */
  angular.module('fraseApp').controller('JobsSingleCtrl', function($scope) {
    return $scope.jobs = [
      {
        id: 1,
        title: 'Roteiro pra cliente XXXXX',
        image: 'images/job01.jpg',
        url: '#/portfolio/producao-de-conteudo-para-actus'
      }, {
        id: 2,
        title: 'Produção de conteúdo para ACTUS',
        image: 'images/job01.jpg',
        url: '#/portfolio/producao-de-conteudo-para-actus'
      }, {
        id: 3,
        title: 'Texto para folder ERIONE',
        image: 'images/job01.jpg',
        url: '#/portfolio/producao-de-conteudo-para-actus'
      }, {
        id: 4,
        title: 'Texto para folder Clínica Médico Fácil',
        image: 'images/job01.jpg',
        url: '#/portfolio/producao-de-conteudo-para-actus'
      }
    ];
  });

}).call(this);

//# sourceMappingURL=jobssingle.js.map
